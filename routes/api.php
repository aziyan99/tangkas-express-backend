<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1', 'middleware' => 'api'], function () {

    Route::post('/logout', [App\Http\Controllers\Api\V1\Backend\AuthController::class, 'logout']);
    Route::post('/refresh', [App\Http\Controllers\Api\V1\Backend\AuthController::class, 'refresh']);
    Route::get('/userProfile', [App\Http\Controllers\Api\V1\Backend\AuthController::class, 'userProfile']);
    Route::put('/updateProfileInfo', [App\Http\Controllers\Api\V1\Backend\ProfileController::class, 'updateProfileInfo']);
    Route::put('/updateProfileImage', [App\Http\Controllers\Api\V1\Backend\ProfileController::class, 'updateProfileImage']);
    Route::put('/updatePassword', [App\Http\Controllers\Api\V1\Backend\ProfileController::class, 'updatePassword']);

    Route::apiResource('/shippingRoute', App\Http\Controllers\Api\V1\Backend\ShippingRouteController::class);
    Route::apiResource('/shippingService', App\Http\Controllers\Api\V1\Backend\ShippingServiceController::class);
    Route::apiResource('/area', App\Http\Controllers\Api\V1\Backend\AreaController::class);
    Route::apiResource('/destination', App\Http\Controllers\Api\V1\Backend\DestinationController::class);
    Route::apiResource('/price', App\Http\Controllers\Api\V1\Backend\PriceController::class);

    Route::apiResource('/shippment', App\Http\Controllers\Api\V1\Backend\ShippmentController::class);
    Route::get('/inShippment', [App\Http\Controllers\Api\V1\Backend\ShippmentController::class, 'getInShippment']);
    Route::get('/outShippment', [App\Http\Controllers\Api\V1\Backend\ShippmentController::class, 'getOutShippment']);
    Route::put('/shippmentUpdateStatus/{id}', [App\Http\Controllers\Api\V1\Backend\ShippmentController::class, 'updateShippmentStatus']);
    Route::put('/shippmentUpdatePaymentStatus/{id}', [App\Http\Controllers\Api\V1\Backend\ShippmentController::class, 'updatePaymentStatus']);
    Route::get('/shippmentPrintReceipt/{id}', [App\Http\Controllers\Api\V1\Backend\ShippmentController::class, 'getShippmentReceipt']);
    Route::apiResource('/tracking', App\Http\Controllers\Api\V1\Backend\TrackingController::class)
        ->except([
            'index',
            'update',
            'show',
            'destroy'
        ]);
    Route::get('/shippmentAvailableTrackingStatus/{id}', [App\Http\Controllers\Api\V1\Backend\ShippmentController::class, 'getShippmentTrackingStatus']);

    Route::apiResource('/user', App\Http\Controllers\Api\V1\Backend\UserController::class);
    Route::post('/resetPassword', [App\Http\Controllers\Api\V1\Backend\UserController::class, 'resetPassword']);
    Route::post('/updateUserStatus', [App\Http\Controllers\Api\V1\Backend\UserController::class, 'updateUserStatus']);
    Route::post('/updateUserAgent', [App\Http\Controllers\Api\V1\Backend\UserController::class, 'updateUserAgent']);
    Route::apiResource('/agent', App\Http\Controllers\Api\V1\Backend\AgentController::class);

    Route::get('/searchSenderName', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'searchSenderName']);
    Route::get('/getDestinationByAreaId', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getDestinationByAreaId']);
    Route::get('/getPriceByAreaDestinationService', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getPriceByAreaDestinationService']);
    Route::get('/getAgentByAreaAndDestination', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getAgentByAreaAndDestination']);
    Route::get('/getAgentByArea', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getAgentByArea']);

    Route::apiResource('/availableService', App\Http\Controllers\Api\V1\Backend\AvailableServiceController::class);
    Route::get('/setting', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'index']);
    Route::put('/setting', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'update']);
    Route::put('/updateLogo', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateLogo']);
    Route::put('/updateBannerOne', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerOne']);
    Route::put('/updateBannerTwo', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerTwo']);
    Route::put('/updateBannerThree', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerThree']);
    Route::put('/updateBannerFour', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerFour']);
    Route::put('/updateBannerService', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerService']);
    Route::put('/updateBannerPrice', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerPrice']);
    Route::put('/updateBannerReceipt', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerReceipt']);
    Route::put('/updateBannerContact', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerContact']);
    Route::put('/updateBannerAbout', [App\Http\Controllers\Api\V1\Backend\SettingController::class, 'updateBannerAbout']);

    Route::get('/sender', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getSender']);
    Route::get('/senderDetail/{id}', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getSenderDetail']);
    Route::post('/printManifest', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'printManifest']);
    Route::get('/getInUndoneShippment', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getInUndoneShippment']);
    Route::get('/getOutUndoneShippment', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getOutUndoneShippment']);

    Route::post('/senderReport', [App\Http\Controllers\Api\V1\Backend\ReportController::class, 'senderReport']);
    Route::post('/shippmentReport', [App\Http\Controllers\Api\V1\Backend\ReportController::class, 'shippmentReport']);
    Route::post('/agentReport', [App\Http\Controllers\Api\V1\Backend\ReportController::class, 'agentReport']);

    Route::get('/contact', [App\Http\Controllers\Api\V1\Backend\ContactController::class, 'index']);
    Route::delete('/contact/{id}', [App\Http\Controllers\Api\V1\Backend\ContactController::class, 'destroy']);

    Route::get('/dashboardData', [App\Http\Controllers\Api\V1\Backend\MiscellaneousController::class, 'getDashboardData']);
});


Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', [App\Http\Controllers\Api\V1\Backend\AuthController::class, 'login']);
    Route::get('/checkShippment', [App\Http\Controllers\Api\V1\Frontend\CheckShippingReceiptController::class, 'index']);
    Route::get('/checkShippmentPrice', [App\Http\Controllers\Api\V1\Frontend\CheckShippingCostController::class, 'index']);
    Route::get('/webInfo', [App\Http\Controllers\Api\V1\Frontend\WebInfoController::class, 'index']);
    Route::get('/getAvailableService', [App\Http\Controllers\Api\V1\Frontend\WebInfoController::class, 'getAvailableService']);
    Route::get('/getService', [App\Http\Controllers\Api\V1\Frontend\CheckShippingCostController::class, 'getService']);
    Route::get('/getArea', [App\Http\Controllers\Api\V1\Frontend\CheckShippingCostController::class, 'getArea']);
    Route::get('/getDestination', [App\Http\Controllers\Api\V1\Frontend\CheckShippingCostController::class, 'getDestinationByAreaId']);
    Route::get('/getShippingRouteById', [App\Http\Controllers\Api\V1\Frontend\CheckShippingCostController::class, 'getShippingRouteById']);
    Route::post('/contact', [App\Http\Controllers\Api\V1\Backend\ContactController::class, 'store']);
});
