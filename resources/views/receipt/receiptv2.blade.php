<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ $shippment->receipt_number }}</title>
        <style>
            * {
                font-family: Helvetica, sans-serif;
                font-size: 9px;
                margin: 0;
                padding: 2;
                box-sizing: border-box;
            }

            table,
            td,
            th {
                border: 0.5px solid #706e6e;
            }

            td {
                vertical-align: top;
                text-align: left;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 0.5px solid #706e6e;
            }

            .receipt-number {
                text-align: center;
            }

            .info {
                box-sizing: border-box;
            }

            .row {
                margin-left: -5px;
                margin-right: 5px;
            }

            .column {
                float: left;
                width: 50%;
            }

            /* Clearfix (clear floats) */
            .row::after {
                content: "";
                clear: both;
                display: table;
            }

            .page_break {
                page-break-before: always;
            }

            @page {
                size: 15cm 10cm;
            }

        </style>
    </head>

    <body>
        <span style="font-size: 6px !important;"><small>Tanggal Cetak {{ date('d-M-Y H:i:s', time()) }}</small></span>
        <table style="border: none !important;">
            <tr style="border: none !important;">
                <td style="border: none !important; width:40% !important;">
                    <img class="logo" src="{{ $setting->logo_pdf }}" style="vertical-align: middle !important;"
                        height="16" />
                </td>
                <td style="border: none !important; width:20% !important; text-align:center;">
                    <strong>RESI PENGIRIMAN</strong>
                </td>
                <td style="border: none !important; width:40% !important; text-align:right;">
                    No . : <br>
                    {{ $shippment->receipt_number }}
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="width: 50% !important;">
                    Penerima
                    <br>
                    {{ $shippment->receiver->name }}
                    <br>
                    {{ $shippment->receiver->address }}
                    <br>
                    Kode pos: {{ $shippment->originAgent->destination->postal_code }}
                    <br><br>
                    Telepon: {{ $shippment->receiver->phone_number }}
                </td>
                <td style="width: 50%">
                    Pengirim
                    <br>
                    {{ $shippment->sender->name }}
                    <br>
                    {{ $shippment->sender->address }}
                    <br>
                    Kode pos: {{ $shippment->destinationAgent->destination->postal_code }}
                    <br><br>
                    Telepon:{{ $shippment->sender->phone_number }}
                </td>
            </tr>
        </table>
        <br>
        <div class="receipt-number">
            @php
            $generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
            @endphp
            <img height="25"
                src="data:image/png;base64,{{ base64_encode($generatorPNG->getBarcode($shippment->receipt_number, $generatorPNG::TYPE_CODE_128)) }}"
                alt="barcode">
            <br>
            <small>{{ $shippment->receipt_number }}</small>
        </div>
        <div class="row">
            <div class="column">
                <table>
                    <tr>
                        <td colspan="4" style="text-align: center;">Informasi Kiriman</td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <span style="font-size: 8px;">Koli</span>
                            <br>
                            <b style="font-size: 8px;">{{ $shippment->colli }}</b>
                        </td>
                        <td style="text-align: center;">
                            <span style="font-size: 8px;">Berat</span>
                            <br>
                            <b>{{ $shippment->payment->weight }} KG</b>
                        </td>
                        <td colspan="2" style="text-align: center;">
                            <span style="font-size: 8px;">Diterima Oleh:</span>
                            <br>
                            <b>-</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center;">
                            <span style="font-size: 8px;">Isi Kiriman</span>
                            <br>
                            <b>{{ $shippment->shipping_type }}</b>
                        </td>
                    </tr>
                </table>
                <span style="font-size: 6px;">Catatan: </span> <span style="font-size: 6px;">{{ $shippment->descriptions }}</span>
            </div>
            <div class="column">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: center;">
                            <span style="font-size: 8px;">Layanan</span>
                            <br>
                            <b>{{ $shippment->shippingService->name }}</b>
                        </td>
                        <td style="text-align: center;">
                            <span style="font-size: 8px;">Rute</span>
                            <br>
                            <b>{{ $shippment->shippingService->shippingRoute->name }}</b>
                        </td>
                        <td style="text-align: center;">
                            <span style="font-size: 8px;">Tanggal & Jam</span><br>
                            <span style="font-size: 8px; word-wrap: break-word;">{{ $shippment->created_at }}</span>
                        </td>
                    </tr>
                </table>
                <table style="font-size: 5px !important; border: none !important;">
                    <tr>
                        <td style="border: none !important;">DISKON</td>
                        <td style="border: none !important;">Rp.</td>
                        <td style="border: none !important; text-align: right !important;">
                            {{ number_format($shippment->payment->discount, 2, ',', '.') }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none !important;">ASURANSI</td>
                        <td style="border: none !important;">Rp.</td>
                        <td style="border: none !important; text-align: right !important;">
                            {{ number_format($shippment->payment->insurance, 2, ',', '.') }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: none !important;">PACKING</td>
                        <td style="border: none !important;">Rp.</td>
                        <td style="border: none !important; text-align: right !important;">
                            {{ number_format($shippment->payment->packing, 2, ',', '.') }}
                        </td>
                    </tr>
                    <tr>
                        <td
                            style="border-bottom: none !important;border-left: none !important;border-right: none !important;">
                            <b style="margin: 0 !important; padding:0 !important;">TOTAL</b></td>
                        <td
                            style="border-bottom: none !important;border-left: none !important;border-right: none !important;">
                            <b style="margin: 0 !important; padding:0 !important;">Rp.</b></td>
                        <td
                            style="border-bottom: none !important;border-left: none !important;border-right: none !important; text-align: right !important;">
                            <b
                                style="margin: 0 !important; padding:0 !important;">{{ number_format($shippment->payment->total_price, 2, ',', '.') }}</b>
                        </td>
                    </tr>
                </table>
                <div style="text-align: right">
                    {!!'<img height="38"
                        src="data:image/png;base64,' . DNS2D::getBarcodePNG('https://tangkas-express.com/tracking?receipt_number=' . $shippment->receipt_number, 'QRCODE', 40, 40) . '"
                        alt="barcode" />' !!}
                    <br>
                    <span style="font-size: 7px !important;">
                        {{ $setting->name }} <br>
                        {{ $setting->address }}<br>
                        {{ $setting->phone_number }}
                    </span>
                </div>
            </div>
        </div>
        <i style="font-size: 5px !important;"><small>Dengan menyerahkan kiriman ke Tangkas-Express, pengirim menyatakan
                bahwa keterangan yang tercetak
                diresi ini adalah benar dan setuju dengan pedoman dan syarat pengiriman Tangkas-Express.</small></i>
    </body>

</html>
