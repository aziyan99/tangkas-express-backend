<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ $shippment->receipt_number }}</title>
        <style>
            * {
                box-sizing: border-box;
                font-family: Helvetica, sans-serif;
            }

            .row {
                margin-left: -5px;
                margin-right: -5px;
            }

            .column {
                float: left;
                width: 50%;
                padding: 5px;
            }

            /* Clearfix (clear floats) */
            .row::after {
                content: "";
                clear: both;
                display: table;
            }

            table,
            td,
            th {
                border: 1px solid #808080;
            }

            td {
                padding-left: 20px;
                padding-top: 20px;
                vertical-align: top;
                text-align: left;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

            span.b {
                display: inline-block;
                padding-left: 40px;
            }

            .text {
                display: inline-block;
            }

        </style>
    </head>

    <body>
        <div class="row">
            <div class="column">
                <table>
                    <tr style="background-color: #d5d2d2;">
                        <td colspan="4" style=" padding: 10px;">
                            No. Pelanggan: <b>{{ $shippment->sender->code }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;text-align: left;">
                            <div style="margin-bottom: 10px;">
                                Dari :
                            </div>
                            <div>
                                {{ $shippment->sender->address }}
                                <br><br>
                                {{ $shippment->sender->name }}
                                <br>
                                {{ $shippment->sender->phone_number }}
                            </div>
                        </td>
                        <td>
                            <div style="margin-bottom: 10px;">
                                Kepada :
                            </div>
                            <div>
                                {{ $shippment->receiver->address }}
                                <br>
                                <br>
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 40px;">
                                        Up
                                    </div>
                                    <div style="display: inline; margin-left: 30px;">
                                        :&nbsp;&nbsp;&nbsp;{{ $shippment->receiver->name  }}
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 53px;">
                                        Telp.
                                    </div>
                                    <div style="display: inline; margin-left: 17px;">
                                        :&nbsp;&nbsp;&nbsp;{{ $shippment->receiver->phone_number  }}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td colspan="2" style="width: 40%;">
                            <div style="text-align: center; margin-top:10px;">
                                @php
                                $generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
                                @endphp

                                <img height="23"
                                    src="data:image/png;base64,{{ base64_encode($generatorPNG->getBarcode($shippment->receipt_number, $generatorPNG::TYPE_CODE_128)) }}"
                                    alt="barcode">
                                <br>
                                <small>{{ $shippment->receipt_number }}</small>
                            </div>
                            <div style="margin-top: 20px; margin-bottom: 20px;">
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 60px;">
                                        No. TT
                                    </div>
                                    <div style="display: inline; margin-left: 30px;">
                                        :
                                    </div>
                                </div>
                                <div>
                                    <div  class="text" style="width: 70px;">
                                        No. REF
                                    </div>
                                    <div style="display: inline; margin-left: 20px;">
                                        :
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #d5d2d2;">
                        <td colspan="4" style=" padding: 10px;">
                            Pengirim
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 50px;">
                                    Tanggal
                                </div>
                                <div style="display: inline; margin-left: 54px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shippment_date  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 28px;">
                                    Koli
                                </div>
                                <div style="display: inline; margin-left: 76px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->colli  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 34px;">
                                    Berat
                                </div>
                                <div style="display: inline; margin-left: 70px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->payment->weight  }} Kg
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 75px;">
                                    Keterangan
                                </div>
                                <div style="display: inline; margin-left: 30px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->descriptions  }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 109px;">
                                    Jenis Kiriman
                                </div>
                                <div style="display: inline; margin-left: 32px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shipping_type  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 110px;">
                                    Jenis Layanan
                                </div>
                                <div style="display: inline; margin-left: 30px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shippingService->name  }}
                                    - {{ $shippment->shippingService->shippingRoute->name  }}
                                </div>
                            </div>
                        </td>
                        <td colspan="2" style="text-align: center;">
                            <div style="margin-bottom: 10px;">

                                Diterima Oleh: <br>
                            </div>
                            Nama dan TTG penerima
                            <br><br><br><br>
                            (...................................) <br><br>
                            <div style="width: 30%; margin-left:30%;text-align: left;">
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Tanggal
                                    </div>
                                    <div style="display: inline; margin-left: 16px;">
                                        :
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Waktu
                                    </div>
                                    <div style="display: inline; margin-left: 28px;">
                                        :
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Status
                                    </div>
                                    <div style="display: inline; margin-left: 28px;">
                                        :
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="b">
                                <img src="{{ $setting->logo_pdf }}" alt="logo" width="150" height="100" />
                            </span>
                            <span class="b">
                                {{ $setting->name }}<br>
                                <small>{{ $setting->phone_number }}</small><br>
                                {{ $setting->address }}
                                <br>
                                <br>
                            </span>
                        </td>
                        <td colspan="2">
                            <i>Bukti penerimaan</i>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="column">
                <table>
                    <tr style="background-color: #d5d2d2;">
                        <td colspan="4" style=" padding: 10px;">
                            No. Pelanggan: <b>{{ $shippment->sender->code }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;text-align: left;">
                            <div style="margin-bottom: 10px;">
                                Dari :
                            </div>
                            <div>
                                {{ $shippment->sender->address }}
                                <br><br>
                                {{ $shippment->sender->name }}
                                <br>
                                {{ $shippment->sender->phone_number }}
                            </div>
                        </td>
                        <td>
                            <div style="margin-bottom: 10px;">
                                Kepada :
                            </div>
                            <div>
                                {{ $shippment->receiver->address }}
                                <br>
                                <br>
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 40px;">
                                        Up
                                    </div>
                                    <div style="display: inline; margin-left: 30px;">
                                        :&nbsp;&nbsp;&nbsp;{{ $shippment->receiver->name  }}
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 53px;">
                                        Telp.
                                    </div>
                                    <div style="display: inline; margin-left: 17px;">
                                        :&nbsp;&nbsp;&nbsp;{{ $shippment->receiver->phone_number  }}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td colspan="2" style="width: 40%;">
                            <div style="text-align: center; margin-top:10px;">
                                @php
                                $generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
                                @endphp

                                <img height="23"
                                    src="data:image/png;base64,{{ base64_encode($generatorPNG->getBarcode($shippment->receipt_number, $generatorPNG::TYPE_CODE_128)) }}"
                                    alt="barcode">
                                <br>
                                <small>{{ $shippment->receipt_number }}</small>
                            </div>
                            <div style="margin-top: 20px; margin-bottom: 20px;">
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 60px;">
                                        No. TT
                                    </div>
                                    <div style="display: inline; margin-left: 30px;">
                                        :
                                    </div>
                                </div>
                                <div>
                                    <div  class="text" style="width: 70px;">
                                        No. REF
                                    </div>
                                    <div style="display: inline; margin-left: 20px;">
                                        :
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #d5d2d2;">
                        <td colspan="4" style=" padding: 10px;">
                            Pengirim
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 50px;">
                                    Tanggal
                                </div>
                                <div style="display: inline; margin-left: 54px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shippment_date  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 28px;">
                                    Koli
                                </div>
                                <div style="display: inline; margin-left: 76px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->colli  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 34px;">
                                    Berat
                                </div>
                                <div style="display: inline; margin-left: 70px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->payment->weight  }} Kg
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 75px;">
                                    Keterangan
                                </div>
                                <div style="display: inline; margin-left: 30px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->descriptions  }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 109px;">
                                    Jenis Kiriman
                                </div>
                                <div style="display: inline; margin-left: 32px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shipping_type  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 110px;">
                                    Jenis Layanan
                                </div>
                                <div style="display: inline; margin-left: 30px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shippingService->name  }}
                                    - {{ $shippment->shippingService->shippingRoute->name  }}
                                </div>
                            </div>
                        </td>
                        <td colspan="2" style="text-align: center;">
                            <div style="margin-bottom: 10px;">

                                Diterima Oleh: <br>
                            </div>
                            Nama dan TTG penerima
                            <br><br><br><br>
                            (...................................) <br><br>
                            <div style="width: 30%; margin-left:30%;text-align: left;">
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Tanggal
                                    </div>
                                    <div style="display: inline; margin-left: 16px;">
                                        :
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Waktu
                                    </div>
                                    <div style="display: inline; margin-left: 28px;">
                                        :
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Status
                                    </div>
                                    <div style="display: inline; margin-left: 28px;">
                                        :
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="b">
                                <img src="{{ $setting->logo_pdf }}" alt="logo" width="150" height="100" />
                            </span>
                            <span class="b">
                                {{ $setting->name }}<br>
                                <small>{{ $setting->phone_number }}</small><br>
                                {{ $setting->address }}
                                <br>
                                <br>
                            </span>
                        </td>
                        <td colspan="2">
                            <i>Bukti penerimaan</i>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="column">

                <table>
                    <tr style="background-color: #d5d2d2;">
                        <td colspan="4" style=" padding: 10px;">
                            No. Pelanggan: <b>{{ $shippment->sender->code }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;text-align: left;">
                            <div style="margin-bottom: 10px;">
                                Dari :
                            </div>
                            <div>
                                {{ $shippment->sender->address }}
                                <br><br>
                                {{ $shippment->sender->name }}
                                <br>
                                {{ $shippment->sender->phone_number }}
                            </div>
                        </td>
                        <td>
                            <div style="margin-bottom: 10px;">
                                Kepada :
                            </div>
                            <div>
                                {{ $shippment->receiver->address }}
                                <br>
                                <br>
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 40px;">
                                        Up
                                    </div>
                                    <div style="display: inline; margin-left: 30px;">
                                        :&nbsp;&nbsp;&nbsp;{{ $shippment->receiver->name  }}
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 53px;">
                                        Telp.
                                    </div>
                                    <div style="display: inline; margin-left: 17px;">
                                        :&nbsp;&nbsp;&nbsp;{{ $shippment->receiver->phone_number  }}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td colspan="2" style="width: 40%;">
                            <div style="text-align: center; margin-top:10px;">
                                @php
                                $generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
                                @endphp

                                <img height="23"
                                    src="data:image/png;base64,{{ base64_encode($generatorPNG->getBarcode($shippment->receipt_number, $generatorPNG::TYPE_CODE_128)) }}"
                                    alt="barcode">
                                <br>
                                <small>{{ $shippment->receipt_number }}</small>
                            </div>
                            <div style="margin-top: 20px; margin-bottom: 20px;">
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 60px;">
                                        No. TT
                                    </div>
                                    <div style="display: inline; margin-left: 30px;">
                                        :
                                    </div>
                                </div>
                                <div>
                                    <div  class="text" style="width: 70px;">
                                        No. REF
                                    </div>
                                    <div style="display: inline; margin-left: 20px;">
                                        :
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #d5d2d2;">
                        <td colspan="4" style=" padding: 10px;">
                            Pengirim
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 50px;">
                                    Tanggal
                                </div>
                                <div style="display: inline; margin-left: 54px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shippment_date  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 28px;">
                                    Koli
                                </div>
                                <div style="display: inline; margin-left: 76px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->colli  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 34px;">
                                    Berat
                                </div>
                                <div style="display: inline; margin-left: 70px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->payment->weight  }} Kg
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 75px;">
                                    Keterangan
                                </div>
                                <div style="display: inline; margin-left: 30px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->descriptions  }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 109px;">
                                    Jenis Kiriman
                                </div>
                                <div style="display: inline; margin-left: 32px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shipping_type  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 110px;">
                                    Jenis Layanan
                                </div>
                                <div style="display: inline; margin-left: 30px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shippingService->name  }}
                                    - {{ $shippment->shippingService->shippingRoute->name  }}
                                </div>
                            </div>
                        </td>
                        <td colspan="2" style="text-align: center;">
                            <div style="margin-bottom: 10px;">

                                Diterima Oleh: <br>
                            </div>
                            Nama dan TTG penerima
                            <br><br><br><br>
                            (...................................) <br><br>
                            <div style="width: 30%; margin-left:30%;text-align: left;">
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Tanggal
                                    </div>
                                    <div style="display: inline; margin-left: 16px;">
                                        :
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Waktu
                                    </div>
                                    <div style="display: inline; margin-left: 28px;">
                                        :
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Status
                                    </div>
                                    <div style="display: inline; margin-left: 28px;">
                                        :
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="b">
                                <img src="{{ $setting->logo_pdf }}" alt="logo" width="150" height="100" />
                            </span>
                            <span class="b">
                                {{ $setting->name }}<br>
                                <small>{{ $setting->phone_number }}</small><br>
                                {{ $setting->address }}
                                <br>
                                <br>
                            </span>
                        </td>
                        <td colspan="2">
                            <i>Bukti penerimaan</i>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="column">

                <table>
                    <tr style="background-color: #d5d2d2;">
                        <td colspan="4" style=" padding: 10px;">
                            No. Pelanggan: <b>{{ $shippment->sender->code }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;text-align: left;">
                            <div style="margin-bottom: 10px;">
                                Dari :
                            </div>
                            <div>
                                {{ $shippment->sender->address }}
                                <br><br>
                                {{ $shippment->sender->name }}
                                <br>
                                {{ $shippment->sender->phone_number }}
                            </div>
                        </td>
                        <td>
                            <div style="margin-bottom: 10px;">
                                Kepada :
                            </div>
                            <div>
                                {{ $shippment->receiver->address }}
                                <br>
                                <br>
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 40px;">
                                        Up
                                    </div>
                                    <div style="display: inline; margin-left: 30px;">
                                        :&nbsp;&nbsp;&nbsp;{{ $shippment->receiver->name  }}
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 53px;">
                                        Telp.
                                    </div>
                                    <div style="display: inline; margin-left: 17px;">
                                        :&nbsp;&nbsp;&nbsp;{{ $shippment->receiver->phone_number  }}
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td colspan="2" style="width: 40%;">
                            <div style="text-align: center; margin-top:10px;">
                                @php
                                $generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
                                @endphp

                                <img height="23"
                                    src="data:image/png;base64,{{ base64_encode($generatorPNG->getBarcode($shippment->receipt_number, $generatorPNG::TYPE_CODE_128)) }}"
                                    alt="barcode">
                                <br>
                                <small>{{ $shippment->receipt_number }}</small>
                            </div>
                            <div style="margin-top: 20px; margin-bottom: 20px;">
                                <div style="margin-bottom: 10px;">
                                    <div class="text" style="width: 60px;">
                                        No. TT
                                    </div>
                                    <div style="display: inline; margin-left: 30px;">
                                        :
                                    </div>
                                </div>
                                <div>
                                    <div  class="text" style="width: 70px;">
                                        No. REF
                                    </div>
                                    <div style="display: inline; margin-left: 20px;">
                                        :
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #d5d2d2;">
                        <td colspan="4" style=" padding: 10px;">
                            Pengirim
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 50px;">
                                    Tanggal
                                </div>
                                <div style="display: inline; margin-left: 54px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shippment_date  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 28px;">
                                    Koli
                                </div>
                                <div style="display: inline; margin-left: 76px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->colli  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 34px;">
                                    Berat
                                </div>
                                <div style="display: inline; margin-left: 70px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->payment->weight  }} Kg
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 75px;">
                                    Keterangan
                                </div>
                                <div style="display: inline; margin-left: 30px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->descriptions  }}
                                </div>
                            </div>
                        </td>
                        <td>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 109px;">
                                    Jenis Kiriman
                                </div>
                                <div style="display: inline; margin-left: 32px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shipping_type  }}
                                </div>
                            </div>
                            <div style="margin-bottom: 10px;">
                                <div class="text" style="width: 110px;">
                                    Jenis Layanan
                                </div>
                                <div style="display: inline; margin-left: 30px;">
                                    :&nbsp;&nbsp;&nbsp;{{ $shippment->shippingService->name  }}
                                    - {{ $shippment->shippingService->shippingRoute->name  }}
                                </div>
                            </div>
                        </td>
                        <td colspan="2" style="text-align: center;">
                            <div style="margin-bottom: 10px;">

                                Diterima Oleh: <br>
                            </div>
                            Nama dan TTG penerima
                            <br><br><br><br>
                            (...................................) <br><br>
                            <div style="width: 30%; margin-left:30%;text-align: left;">
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Tanggal
                                    </div>
                                    <div style="display: inline; margin-left: 16px;">
                                        :
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Waktu
                                    </div>
                                    <div style="display: inline; margin-left: 28px;">
                                        :
                                    </div>
                                </div>
                                <div style="margin-bottom: 10px;">
                                    <div style="display: inline;">
                                        Status
                                    </div>
                                    <div style="display: inline; margin-left: 28px;">
                                        :
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="b">
                                <img src="{{ $setting->logo_pdf }}" alt="logo" width="150" height="100" />
                            </span>
                            <span class="b">
                                {{ $setting->name }}<br>
                                <small>{{ $setting->phone_number }}</small><br>
                                {{ $setting->address }}
                                <br>
                                <br>
                            </span>
                        </td>
                        <td colspan="2">
                            <i>Bukti penerimaan</i>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>

</html>
