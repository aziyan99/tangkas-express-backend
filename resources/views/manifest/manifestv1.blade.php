<!doctype html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ $title }}</title>
        <style>
            * {
                font-family: Helvetica, sans-serif;
            }

            table,
            td,
            th {
                border: 1px solid #808080;
            }

            td {
                padding-left: 20px;
                padding-top: 20px;
                padding-bottom: 20px;
                vertical-align: top;
                text-align: left;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

        </style>
    </head>

    <body>
        <div>
            <table>
                <tr>
                    <td style="width: 300px;">
                        <h3>{{ $originAgent->name }}</h3>
                        <p>{{ $originAgent->address }}</p>
                        <p>{{ $originAgent->phone_number }}</p>
                    </td>
                    <td style="width: 300px; border-top: 1px solid #ffffff; border-bottom: none; text-align: center;">

                        <img src="{{ $setting->logo_pdf }}" alt="logo" width="150" height="100" />
                        <h3><u>Manifest Pengiriman</u></h3>
                        <small>Tanggal cetak: {{ date('d-m-Y', time()) }}</small>
                    </td>
                    <td style="text-align: left; padding: 10px; width: 300px;">
                        <h3>{{ $destinationAgent->name }}</h3>
                        <p>{{ $destinationAgent->address }}</p>
                        <p>{{ $originAgent->phone_number }}</p>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding: 10px;">
                        Petugas sortir&nbsp;&nbsp;:
                    </td>
                    <td style="border-top: 0; border-bottom: 0;">

                    </td>
                    <td style="text-align: left; padding: 10px;">
                        Petugas sortir&nbsp;&nbsp;:
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; padding: 10px;">
                        Tanggal&nbsp;&nbsp;:
                    </td>
                    <td style="width: 300px; border-top: 1px solid #ffffff; border-bottom: 1px solid #ffffff;">

                    </td>
                    <td style="text-align: left; padding: 10px;">
                        Tanggal&nbsp;&nbsp;:
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table>
                <tr style="background-color: #d5d2d2;">
                    <td style="padding: 10px;"> No. Resi </td>
                    <td style="padding: 10px;"> No. Kargo </td>
                    <td style="padding: 10px;"> Pengirim </td>
                    <td style="padding: 10px;"> Penerima </td>
                    <td style="padding: 10px;"> Tujuan </td>
                    <td style="padding: 10px;"> Alamat Tujuan </td>
                    <td style="padding: 10px;"> Layanan </td>
                    <td style="padding: 10px;"> Berat </td>
                    <td style="padding: 10px;"> Koli </td>
                    <td style="padding: 10px;"> Jenis Kiriman </td>
                    <td style="padding: 10px;"> Keterangan </td>
                </tr>
                @foreach($shippments as $shippment)
                <tr>
                    <td> {{ $shippment->receipt_number }} </td>
                    <td> {{ $shippment->cargo_number }} </td>
                    <td> {{ $shippment->sender->name }} </td>
                    <td> {{ $shippment->receiver->name }} </td>
                    <td> {{ $shippment->destinationAgent->destination->name }} </td>
                    <td> {{ $shippment->receiver->address }} </td>
                    <td>
                        {{ $shippment->shippingService->name }} -
                        {{ $shippment->shippingService->shippingRoute->name }}
                    </td>
                    <td> {{ $shippment->payment->weight }} Kg</td>
                    <td> {{ $shippment->colli }} </td>
                    <td> {{ $shippment->shipping_type }} </td>
                    <td> {{ $shippment->descriptions }} </td>
                </tr>
                @endforeach
            </table>
        </div>
    </body>

</html>
