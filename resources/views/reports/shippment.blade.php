<!doctype html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ $title }}</title>
        <style>
            * {
                font-family: Helvetica, sans-serif;
            }

            table,
            td,
            th {
                border: 1px solid #808080;
            }

            td {
                padding-left: 20px;
                padding-top: 20px;
                padding-bottom: 20px;
                vertical-align: top;
                text-align: left;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

        </style>
    </head>

    <body>
        <table>
            <tr style="background-color: #d5d2d2;">
                <td style="padding: 10px;"> No. Resi </td>
                <td style="padding: 10px;"> Pengirim </td>
                <td style="padding: 10px;"> Penerima </td>
                <td style="padding: 10px;"> Agent Pengirim </td>
                <td style="padding: 10px;"> Agent Penerima </td>
                <td style="padding: 10px;"> Berat </td>
                <td style="padding: 10px;"> Status Pengiriman </td>
                <td style="spadding: 10px;"> Status Pembayaran </td>
            </tr>
            @foreach($shippments as $shippment)
            <tr>
                <td> {{ $shippment->receipt_number }} </td>
                <td> {{ $shippment->sender->name }} </td>
                <td> {{ $shippment->receiver->name }} </td>
                <td> {{ $shippment->originAgent->name }} </td>
                <td> {{ $shippment->destinationAgent->name }} </td>
                <td> {{ $shippment->payment->weight }} </td>
                <td> {{ $shippment->status }} </td>
                <td> {{ $shippment->payment->status }} </td>
            </tr>
            @endforeach
        </table>
    </body>

</html>
