<!doctype html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ $title }}</title>
        <style>
            * {
                font-family: Helvetica, sans-serif;
            }

            table,
            td,
            th {
                border: 1px solid #808080;
            }

            td {
                padding-left: 20px;
                padding-top: 20px;
                padding-bottom: 20px;
                vertical-align: top;
                text-align: left;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

        </style>
    </head>

    <body>
        <table>
            <tr style="background-color: #d5d2d2;">
                <td style="padding: 10px;"> No. </td>
                <td style="padding: 10px;"> Code </td>
                <td style="padding: 10px;"> Nama </td>
                <td style="padding: 10px;"> No.Hp </td>
                <td style="padding: 10px;"> Alamat </td>
            </tr>
            @foreach($senders as $sender)
            <tr>
                <td> {{ $loop->iteration }} </td>
                <td> {{ $sender->code }} </td>
                <td> {{ $sender->name }} </td>
                <td> {{ $sender->phone_number }} </td>
                <td> {{ $sender->address }} </td>
            </tr>
            @endforeach
        </table>
    </body>

</html>
