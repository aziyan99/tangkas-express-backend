<!doctype html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ $title }}</title>
        <style>
            * {
                font-family: Helvetica, sans-serif;
            }

            table,
            td,
            th {
                border: 1px solid #808080;
            }

            td {
                padding-left: 20px;
                padding-top: 20px;
                padding-bottom: 20px;
                vertical-align: top;
                text-align: left;
            }

            table {
                border-collapse: collapse;
                border-spacing: 0;
                width: 100%;
                border: 1px solid #ddd;
            }

        </style>
    </head>

    <body>
        <table>
            <tr style="background-color: #d5d2d2;">
                <td style="padding: 10px;"> Kode </td>
                <td style="padding: 10px;"> Area </td>
                <td style="padding: 10px;"> Destinasi </td>
                <td style="padding: 10px;"> Alamat </td>
                <td style="padding: 10px;"> Tanggal Dibuat </td>
            </tr>
            @foreach($agents as $agent)
            <tr>
                <td> {{ $agent->name }} </td>
                <td> {{ $agent->area->name }} </td>
                <td> {{ $agent->destination->name }} </td>
                <td> {{ $agent->address }} </td>
                <td> {{ $agent->created_at->format('d/m/Y') }} </td>
            </tr>
            @endforeach
        </table>
    </body>

</html>
