<?php

namespace Tests\Feature;

use App\Models\Area;
use App\Models\Destination;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DestinationTest extends TestCase
{
    use RefreshDatabase;

    public function test_create()
    {
        $area = Area::create([
            'code' => 'BTM',
            'name' => 'Batam',
            'descriptions' => '-'
        ]);

        $response = $this->json('POST', 'api/v1/destination', [
            'area_id' => $area->id,
            'code' => 'BTHJ',
            'name' => 'Batu Aji',
            'descriptions' => '-'
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(201);
    }

    public function test_read_all()
    {
        $response = $this->json('GET', 'api/v1/destination', [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
    }

    public function test_read_by_id()
    {
        $area = Area::create([
            'code' => 'BTM',
            'name' => 'Batam',
            'descriptions' => '-'
        ]);
        $destination = Destination::create([
            'area_id' => $area->id,
            'code' => 'BTAJ',
            'name' => 'Batu aji',
            'descriptions' => '-'
        ]);
        $response = $this->json('GET', 'api/v1/destination', [
            'Accept' => 'application/json'
        ]);

        $response->assertSeeText('BTAJ');
    }

    public function test_update()
    {
        $area = Area::create([
            'code' => 'BTM',
            'name' => 'Batam',
            'descriptions' => '-'
        ]);
        $destination = Destination::create([
            'area_id' => $area->id,
            'code' => 'BTAJ',
            'name' => 'Batu aji',
            'descriptions' => '-'
        ]);
        $response = $this->json('PUT', 'api/v1/destination/' . $destination->id, [
            'area_id' => $area->id,
            'code' => 'BTMCNTR',
            'name' => 'Batam Center',
            'descriptions' => '-'
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
    }

    public function test_delete()
    {
        $area = Area::create([
            'code' => 'BTM',
            'name' => 'Batam',
            'descriptions' => '-'
        ]);
        $destination = Destination::create([
            'area_id' => $area->id,
            'code' => 'BTAJ',
            'name' => 'Batu aji',
            'descriptions' => '-'
        ]);
        $response = $this->json('DELETE', 'api/v1/destination/' . $destination->id, [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
    }
}
