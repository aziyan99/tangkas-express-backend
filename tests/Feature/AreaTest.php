<?php

namespace Tests\Feature;

use App\Models\Area;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AreaTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_area_can_be_create()
    {
        $response = $this->json('POST', 'api/v1/area', [
            'code' => 'PDG',
            'name' => 'Padang',
            'descriptions' => '-'

        ], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(201);
    }


    public function test_get_all_area_data()
    {
        $response = $this->json('GET', 'api/v1/area', [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(200);
    }


    public function test_update_area()
    {
        $area = Area::create([
            'code' => 'PDG',
            'name' => 'Padang',
            'descriptions' => '-'
        ]);

        $response = $this->json('PUT', 'api/v1/area/' . $area->id, [
            'code' => 'BTM',
            'name' => 'Batam',
            'descriptions' => '-'

        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
    }

    public function test_get_area_by_id()
    {
        $area = Area::create([
            'code' => 'PDG',
            'name' => 'Padang',
            'descriptions' => '-'
        ]);
        $response = $this->json('GET', 'api/v1/area/' . $area->id, [
            'Accept' => 'application/json'
        ]);
        $response->assertSeeText('PDG');
    }

    public function test_delete_area()
    {
        $area = Area::create([
            'code' => 'PDG',
            'name' => 'Padang',
            'descriptions' => '-'
        ]);
        $response = $this->json('DELETE', 'api/v1/area/' . $area->id, [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(200);
    }
}
