<?php

namespace Tests\Feature;

use App\Models\ShippingRoute;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ShippingRouteTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_shipping_route_can_be_create()
    {
        $response = $this->json('POST', 'api/v1/shippingRoute', [
            'name' => 'Darat',
            'descriptions' => 'Pengiriman lewat jalur darat'

        ], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(201);
    }


    public function test_an_error_message_when_field_empty()
    {
        $response = $this->json('POST', 'api/v1/shippingRoute', [
            'name' => 'Darat',
            'descriptions' => ''

        ], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(422);
    }


    public function test_get_all_shipping_route_data()
    {
        $response = $this->json('GET', 'api/v1/shippingRoute', [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(200);
    }


    public function test_update_shipping_route()
    {
        $shippingRoute = ShippingRoute::create([
            'name' => 'Darat',
            'descriptions' => "Pengiriman lewat jalur darat"
        ]);

        $response = $this->json('PUT', 'api/v1/shippingRoute/' . $shippingRoute->id, [
            'name' => 'Laut',
            'descriptions' => 'Pengiriman lewat jalur laut'

        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertSeeText('Shipping route updated');
    }

    public function test_get_shipping_route_by_id()
    {
        $shippingRoute = ShippingRoute::create([
            'name' => 'Darat',
            'descriptions' => "Pengiriman lewat jalur darat"
        ]);
        $response = $this->json('GET', 'api/v1/shippingRoute/'. $shippingRoute->id, [
            'Accept' => 'application/json'
        ]);
        $response->assertSeeText('Darat');
    }

    public function test_delete_shipping_route()
    {
        $shippingRoute = ShippingRoute::create([
            'name' => 'Darat',
            'descriptions' => "Pengiriman lewat jalur darat"
        ]);
        $response = $this->json('DELETE', 'api/v1/shippingRoute/'. $shippingRoute->id, [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(200);
    }
}
