<?php

namespace Tests\Feature;

use App\Models\ShippingService;
use App\Models\ShippingRoute;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShippingServiceTest extends TestCase
{
    use RefreshDatabase;

    public function test_a_shipping_service_can_be_create()
    {
        $shippingRoute = ShippingRoute::create([
            'name' => 'Darat',
            'descriptions' => '--'
        ]);
        $response = $this->json('POST', 'api/v1/shippingService', [
            'shipping_route_id' => $shippingRoute->id,
            'name' => 'Darat',
            'descriptions' => 'Pengiriman lewat jalur darat'

        ], [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(201);
    }


    public function test_get_all_shipping_service_data()
    {
        $response = $this->json('GET', 'api/v1/shippingService', [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(200);
    }


    public function test_update_shipping_service()
    {
        $shippingRoute = ShippingRoute::create([
            'name' => 'Darat',
            'descriptions' => '--'
        ]);
        $shippingService = ShippingService::create([
            'shipping_route_id' => $shippingRoute->id,
            'name' => 'Express',
            'descriptions' => 'Pengiriman lewat jalur darat express'
        ]);

        $response = $this->json('PUT', 'api/v1/shippingService/' . $shippingService->id, [
            'shipping_route_id' => $shippingRoute->id,
            'name' => 'Reguler',
            'descriptions' => 'Pengiriman lewat jalur laut reguler'

        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
    }

    public function test_get_shipping_service_by_id()
    {
        $shippingRoute = ShippingRoute::create([
            'name' => 'Darat',
            'descriptions' => '--'
        ]);
        $shippingService = ShippingService::create([
            'shipping_route_id' => $shippingRoute->id,
            'name' => 'Express',
            'descriptions' => 'Pengiriman lewat jalur darat express'
        ]);
        $response = $this->json('GET', 'api/v1/shippingService/' . $shippingService->id, [
            'Accept' => 'application/json'
        ]);
        $response->assertSeeText('Express');
    }

    public function test_delete_shipping_service()
    {
        $shippingRoute = ShippingRoute::create([
            'name' => 'Darat',
            'descriptions' => '--'
        ]);
        $shippingService = ShippingService::create([
            'shipping_route_id' => $shippingRoute->id,
            'name' => 'Express',
            'descriptions' => 'Pengiriman lewat jalur darat express'
        ]);
        $response = $this->json('DELETE', 'api/v1/shippingService/' . $shippingService->id, [
            'Accept' => 'application/json'
        ]);
        $response->assertStatus(200);
    }
}
