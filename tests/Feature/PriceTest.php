<?php

namespace Tests\Feature;

use App\Models\ShippingRoute;
use App\Models\ShippingService;
use App\Models\Area;
use App\Models\Destination;
use App\Models\Price;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PriceTest extends TestCase
{
    use RefreshDatabase;

    public function test_create()
    {
        $prepareData = $this->prepareData();
        $response = $this->json('POST', 'api/v1/price', [
            'shipping_service_id' => $prepareData['shipping_service_id'],
            'area_id' => $prepareData['area_id'],
            'destination_id' => $prepareData['destination_id'],
            'min_weight' => '8',
            'price' => '20000',
            'lead_time' => '12',
            'descriptions' => '-',
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(201);
    }

    public function test_read_all()
    {
        $response = $this->json('GET', 'api/v1/price', [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
    }

    public function test_read_by_id()
    {
        $prepareData = $this->prepareData();
        $price = Price::create([
            'shipping_service_id' => $prepareData['shipping_service_id'],
            'area_id' => $prepareData['area_id'],
            'destination_id' => $prepareData['destination_id'],
            'min_weight' => '8',
            'price' => '20000',
            'lead_time' => '12',
            'descriptions' => '-',
        ]);
        $response = $this->json('GET', 'api/v1/price/' . $price->id, [
            'Accept' => 'application/json'
        ]);

        $response->assertSeeText('12');
    }

    public function test_update()
    {
        $prepareData = $this->prepareData();
        $price = Price::create([
            'shipping_service_id' => $prepareData['shipping_service_id'],
            'area_id' => $prepareData['area_id'],
            'destination_id' => $prepareData['destination_id'],
            'min_weight' => '8',
            'price' => '20000',
            'lead_time' => '12',
            'descriptions' => '-',
        ]);
        $response = $this->json('PUT', 'api/v1/price/' . $price->id, [
            'shipping_service_id' => $prepareData['shipping_service_id'],
            'area_id' => $prepareData['area_id'],
            'destination_id' => $prepareData['destination_id'],
            'min_weight' => '10',
            'price' => '50000',
            'lead_time' => '25',
            'descriptions' => '-',
        ], [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
    }

    public function test_delete()
    {
        $prepareData = $this->prepareData();
        $price = Price::create([
            'shipping_service_id' => $prepareData['shipping_service_id'],
            'area_id' => $prepareData['area_id'],
            'destination_id' => $prepareData['destination_id'],
            'min_weight' => '8',
            'price' => '20000',
            'lead_time' => '12',
            'descriptions' => '-',
        ]);
        $response = $this->json('DELETE', 'api/v1/price/' . $price->id, [
            'Accept' => 'application/json'
        ]);

        $response->assertStatus(200);
    }

    public function prepareData()
    {
        $shippingRoute = ShippingRoute::create([
            'name' => 'Darat',
            'descriptions' => '-'
        ]);
        $shippingService = ShippingService::create([
            'shipping_route_id' => $shippingRoute->id,
            'name' => 'Express',
            'descriptions' => 'Pengiriman lewat jalur darat express'
        ]);
        $area = Area::create([
            'code' => 'BTM',
            'name' => 'Batam',
            'descriptions' => '-'
        ]);
        $destination = Destination::create([
            'area_id' => $area->id,
            'code' => 'BTAJ',
            'name' => 'Batu aji',
            'descriptions' => '-'
        ]);

        $data = [
            'shipping_service_id' => $shippingService->id,
            'area_id' => $area->id,
            'destination_id' => $destination->id,
        ];

        return $data;
    }
}
