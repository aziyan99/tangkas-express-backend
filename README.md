## Tangkas Express
> An rest api for tangkas-express

### Todos
- [x] Generate sender code if the code not exists in request
- [x] Authorization
  - [x] Admin
  - [x] Agent
- [x] Report
- [x] Dashboard
- [x] Profile page
- [x] Ansuransi
- [x] Diskon
- [x] Manifest
  - [x] print pdf laporan
- [x] Update image path
- [x] Update password
- [x] Gitlab account (login with github)
- [x] Github account
- [x] DB Transaction
- [x] Update resi
- [x] Update the logic how sender code generate when address and phone number change
- [x] Add non-activate users
- [x] Add packing price
- [x] Add process to update ever landing page image
- [x] Modify shipping response data
- [ ] Test all functionality
- [ ] Add detail report to shippment
- [ ] Write testing

### How to install
Below is the steps how to install the rest api for tangkas-express
1. Update depedencies using `composer install`
2. Copy .env.example to .env `cp -r .env.example .env`
3. Update the database configuration on `.env` file in database configuration section
   ```
    DB_CONNECTION=
    DB_HOST=
    DB_PORT=
    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=
   ```
4. Generate storage link `php artisan storage:link`
5. Generate key `php artisan generate:key`
6. Migration the database and initial data `php artisan migrate:fresh --seed`
7. Clear config cache `php aritsan cache:clear`
8. Generate JWT Secret `php artisan jwt:secret`
9. Add JWT TTL under `.env` file `JWT_TTL = 240` 240 in minute

### Default authentication credential for administrator
| Email             | Password    |
| ----------------- | ------------|
| admin@example.com | password    |

### Developers
1. Fernando
2. Dimas
3. Ezzy
