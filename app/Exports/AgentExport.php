<?php

namespace App\Exports;


use App\Models\Agent;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AgentExport implements FromView
{
    public $title;

    public function __construct(string $title) {
        $this->title = $title;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $agents = Agent::with('area', 'destination')->latest()->get();
        $title = $this->title;
        return view('reports.agent', compact('agents', 'title'));
    }
}
