<?php

namespace App\Exports;


use App\Models\Sender;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SenderExport implements FromView
{
    public $startDate;
    public $endDate;
    public $title;

    public function __construct(string $startDate, string $endDate, string $title)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->title = $title;
    }

    /**
     * @return View
     */
    public function view(): View
    {

        $startDate = Carbon::createFromFormat('Y-m-d', $this->startDate);
        $endDate = Carbon::createFromFormat('Y-m-d', $this->endDate);
        $senders = Sender::whereBetween('created_at', [$startDate, $endDate])->get();
        $title = $this->title;
        return view('reports.sender', compact('senders', 'title'));
    }
}
