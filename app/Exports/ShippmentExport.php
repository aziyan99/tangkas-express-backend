<?php

namespace App\Exports;


use App\Models\Shippment;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ShippmentExport implements FromView
{
    public $startDate;
    public $endDate;
    public $title;

    public function __construct(string $startDate, string $endDate, string $title)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->title = $title;
    }

    /**
     * @return View
     */
    public function view(): View
    {

        $startDate = Carbon::createFromFormat('Y-m-d', $this->startDate);
        $endDate = Carbon::createFromFormat('Y-m-d', $this->endDate);
        $shippments = Shippment::with(
            'originAgent',
            'destinationAgent',
            'sender',
            'receiver',
            'payment'
        )
            ->whereBetween('created_at', [$startDate, $endDate])->get();
        $title = $this->title;
        return view('reports.shippment', compact('shippments', 'title'));
    }
}
