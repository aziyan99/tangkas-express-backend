<?php

namespace App\Http\Controllers\Api\V1\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Shippment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CheckShippingReceiptController extends Controller
{

    /**
     * Check shippment status
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $request->validate([
            'receipt_number' => 'required'
        ]);

        $shippingReceipt = Shippment::with('shippingService', 'sender', 'receiver', 'trackings')
            ->where('receipt_number', $request->receipt_number)
            ->first();
        if (!$shippingReceipt) {
            return response()->json([
                'success' => false,
                'message' => 'Shipping receipt not found',
                'data' => []
            ], 404);
        }else {
            return response()->json([
                'success' => true,
                'message' => 'Shipping receipt data',
                'data' => $shippingReceipt
            ], 200);
        }
    }
}
