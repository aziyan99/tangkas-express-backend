<?php

namespace App\Http\Controllers\Api\V1\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Destination;
use App\Models\Price;
use App\Models\ShippingRoute;
use App\Models\ShippingService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CheckShippingCostController extends Controller
{

    /**
     * Check shipping cost
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $request->validate([
            'area_id' => 'required',
            'destination_id' => 'required',
            'service_id' => 'required'
        ]);

        $areaId = $request->area_id;
        $destinationId = $request->destination_id;
        $serviceId = $request->service_id;

        $price = Price::with('shippingService', 'area', 'destination')
            ->where('area_id', $areaId)
            ->where('destination_id', $destinationId)
            ->where('shipping_service_id', $serviceId)
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Price data',
            'data' => $price
        ], 200);
    }

    /**
     * Get all area data
     *
     * @return JsonResponse
     */
    public function getArea(): JsonResponse
    {
        $areas = Area::with('destinations')->latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Areas data',
            'data' => $areas
        ], 200);
    }

    /**
     * Get available destinations in area
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getDestinationByAreaId(Request $request): JsonResponse
    {
        $areaId = $request->area_id;
        $destinations = Destination::with('area')->where('area_id', $areaId)->get();
        return response()->json([
            'success' => true,
            'message' => 'Destinations data',
            'data' => $destinations
        ], 200);
    }

    /**
     * Get all services data
     *
     * @return JsonResponse
     */
    public function getService(): JsonResponse
    {
        $shippingService = ShippingService::with('shippingRoute')->latest()->get();
        return response()->json([
            "success" => true,
            "message" => "Shipping service data",
            "data" => $shippingService
        ], 200);
    }

    public function getShippingRouteById(Request $request)
    {
        $request->validate([
            'shipping_service_id' => 'required'
        ]);
        $shippingService = ShippingService::findOrFail($request->shipping_service_id);
        $shippingRoutes = ShippingRoute::findOrFail($shippingService->shipping_route_id);
        return response()->json([
            "success" => true,
            "message" => "Shipping service data",
            "data" => $shippingRoutes
        ], 200);
    }
}
