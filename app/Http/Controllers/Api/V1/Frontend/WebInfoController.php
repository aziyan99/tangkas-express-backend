<?php

namespace App\Http\Controllers\Api\V1\Frontend;

use App\Http\Controllers\Controller;
use App\Models\AvailableService;
use App\Models\Setting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class WebInfoController extends Controller
{

    /**
     * Get web info data
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $setting = Setting::first();
        return response()->json([
            'success' => true,
            'message' => 'Web data',
            'data' => $setting
        ], 200);
    }

    /**
     * Get available service
     *
     * @return JsonResponse
     */
    public function getAvailableService(): JsonResponse
    {
        $services = AvailableService::all();
        return response()->json([
            'success' => true,
            'message' => 'Available services data',
            'data' => $services
        ], 200);
    }
}
