<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SettingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the setting data.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $setting = Setting::first();
        return response()->json([
            'success' => true,
            'message' => 'Setting data',
            'data' => $setting
        ], 200);
    }

    /**
     * Update setting data from storage
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'phone_number' => 'required',
            'email' => 'required',
            'instagram' => 'required',
            'facebook' => 'required',
            'address' => 'required',
            'name' => 'required',
            'visi' => 'required',
            'misi' => 'required',
            'privacy_policy' => 'required',
            'about' => 'required'
        ]);

        $setting = Setting::first();
        $setting->update([
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'address' => $request->address,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'name' => $request->name,
            'visi' => $request->visi,
            'misi' => $request->misi,
            'privacy_policy' => $request->privacy_policy,
            'about' => $request->about,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    /**
     * Update logo from storage
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateLogo(Request $request): JsonResponse
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }

        $request->validate([
            'logo' => 'required'
        ]);

        $setting = Setting::first();
        //convert base64 to image and save it
        $image_64 = $request->logo;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        // find substring for replace here eg: data:image/png;base64,
        $logo = str_replace($replace, '', $image_64);
        $logo = str_replace(' ', '+', $logo);
        $imageName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/logo/' . $imageName, base64_decode($logo));

        if (Storage::disk('public')->exists($setting->logo)) {
            Storage::disk('pulic')->delete($setting->logo);
        }

        $setting->update([
            'logo' => '/logo/' . $imageName,
            'logo_pdf' => '/logo/' . $imageName,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }


    /**
     * Update banner
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateBannerOne(Request $request): JsonResponse
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }

        $request->validate([
            'banner_1' => 'required',
        ]);
        $setting = Setting::first();

        //banner 1
        $bannerImage1_64 = $request->banner_1;
        $extension = explode('/', explode(':', substr($bannerImage1_64, 0, strpos($bannerImage1_64, ';')))[1])[1];
        $replace = substr($bannerImage1_64, 0, strpos($bannerImage1_64, ',') + 1);
        $banner1 = str_replace($replace, '', $bannerImage1_64);
        $banner1 = str_replace(' ', '+', $banner1);
        $banner1Name = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $banner1Name, base64_decode($banner1));

        if (Storage::disk('public')->exists($setting->banner_1)) {
            Storage::disk('pulic')->delete($setting->banner_1);
        }



        $setting->update([
            'banner_one' => '/banners/' . $banner1Name,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    public function updateBannerTwo(Request $request)
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'banner_2' => 'required',
        ]);
        $setting = Setting::first();
        //banner 2
        $bannerImage2_64 = $request->banner_2;
        $extension = explode('/', explode(':', substr($bannerImage2_64, 0, strpos($bannerImage2_64, ';')))[1])[1];
        $replace = substr($bannerImage2_64, 0, strpos($bannerImage2_64, ',') + 1);
        $banner2 = str_replace($replace, '', $bannerImage2_64);
        $banner2 = str_replace(' ', '+', $banner2);
        $banner2Name = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $banner2Name, base64_decode($banner2));

        if (Storage::disk('public')->exists($setting->banner_2)) {
            Storage::disk('pulic')->delete($setting->banner_2);
        }

        $setting->update([
            'banner_two' => '/banners/' . $banner2Name,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    public function updateBannerThree(Request $request)
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'banner_3' => 'required',
        ]);
        $setting = Setting::first();

        //banner 3
        $bannerImage3_64 = $request->banner_3;
        $extension = explode('/', explode(':', substr($bannerImage3_64, 0, strpos($bannerImage3_64, ';')))[1])[1];
        $replace = substr($bannerImage3_64, 0, strpos($bannerImage3_64, ',') + 1);
        $banner3 = str_replace($replace, '', $bannerImage3_64);
        $banner3 = str_replace(' ', '+', $banner3);
        $banner3Name = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $banner3Name, base64_decode($banner3));

        if (Storage::disk('public')->exists($setting->banner_3)) {
            Storage::disk('pulic')->delete($setting->banner_3);
        }

        $setting->update([
            'banner_three' => '/banners/' . $banner3Name,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    public function updateBannerFour(Request $request)
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'banner_4' => 'required'
        ]);
        $setting = Setting::first();

        //banner 4
        $bannerImage4_64 = $request->banner_4;
        $extension = explode('/', explode(':', substr($bannerImage4_64, 0, strpos($bannerImage4_64, ';')))[1])[1];
        $replace = substr($bannerImage4_64, 0, strpos($bannerImage4_64, ',') + 1);
        $banner4 = str_replace($replace, '', $bannerImage4_64);
        $banner4 = str_replace(' ', '+', $banner4);
        $banner4Name = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $banner4Name, base64_decode($banner4));

        if (Storage::disk('public')->exists($setting->banner_4)) {
            Storage::disk('pulic')->delete($setting->banner_4);
        }

        $setting->update([
            'banner_four' => '/banners/' . $banner4Name,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    public function updateBannerService(Request $request)
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'service_banner' => 'required'
        ]);
        $setting = Setting::first();

        //banner 4
        $bannerService_64 = $request->service_banner;
        $extension = explode('/', explode(':', substr($bannerService_64, 0, strpos($bannerService_64, ';')))[1])[1];
        $replace = substr($bannerService_64, 0, strpos($bannerService_64, ',') + 1);
        $bannerService = str_replace($replace, '', $bannerService_64);
        $bannerService = str_replace(' ', '+', $bannerService);
        $bannerServiceName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $bannerServiceName, base64_decode($bannerService));

        if (Storage::disk('public')->exists($setting->service_banner)) {
            Storage::disk('pulic')->delete($setting->service_banner);
        }

        $setting->update([
            'service_banner' => '/banners/' . $bannerServiceName,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    public function updateBannerPrice(Request $request)
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'price_banner' => 'required'
        ]);
        $setting = Setting::first();

        //banner 4
        $banner64 = $request->price_banner;
        $extension = explode('/', explode(':', substr($banner64, 0, strpos($banner64, ';')))[1])[1];
        $replace = substr($banner64, 0, strpos($banner64, ',') + 1);
        $banner = str_replace($replace, '', $banner64);
        $banner = str_replace(' ', '+', $banner);
        $bannerName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $bannerName, base64_decode($banner));

        if (Storage::disk('public')->exists($setting->price_banner)) {
            Storage::disk('pulic')->delete($setting->price_banner);
        }

        $setting->update([
            'price_banner' => '/banners/' . $bannerName,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    public function updateBannerReceipt(Request $request)
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'receipt_banner' => 'required'
        ]);
        $setting = Setting::first();

        //banner 4
        $banner64 = $request->receipt_banner;
        $extension = explode('/', explode(':', substr($banner64, 0, strpos($banner64, ';')))[1])[1];
        $replace = substr($banner64, 0, strpos($banner64, ',') + 1);
        $banner = str_replace($replace, '', $banner64);
        $banner = str_replace(' ', '+', $banner);
        $bannerName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $bannerName, base64_decode($banner));

        if (Storage::disk('public')->exists($setting->receipt_banner)) {
            Storage::disk('pulic')->delete($setting->receipt_banner);
        }

        $setting->update([
            'receipt_banner' => '/banners/' . $bannerName,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    public function updateBannerContact(Request $request)
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'contact_banner' => 'required'
        ]);
        $setting = Setting::first();

        //banner 4
        $banner64 = $request->contact_banner;
        $extension = explode('/', explode(':', substr($banner64, 0, strpos($banner64, ';')))[1])[1];
        $replace = substr($banner64, 0, strpos($banner64, ',') + 1);
        $banner = str_replace($replace, '', $banner64);
        $banner = str_replace(' ', '+', $banner);
        $bannerName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $bannerName, base64_decode($banner));

        if (Storage::disk('public')->exists($setting->contact_banner)) {
            Storage::disk('pulic')->delete($setting->contact_banner);
        }

        $setting->update([
            'contact_banner' => '/banners/' . $bannerName,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }

    public function updateBannerAbout(Request $request)
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'about_banner' => 'required'
        ]);
        $setting = Setting::first();

        //banner 4
        $banner64 = $request->about_banner;
        $extension = explode('/', explode(':', substr($banner64, 0, strpos($banner64, ';')))[1])[1];
        $replace = substr($banner64, 0, strpos($banner64, ',') + 1);
        $banner = str_replace($replace, '', $banner64);
        $banner = str_replace(' ', '+', $banner);
        $bannerName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/banners/' . $bannerName, base64_decode($banner));

        if (Storage::disk('public')->exists($setting->about_banner)) {
            Storage::disk('pulic')->delete($setting->about_banner);
        }

        $setting->update([
            'about_banner' => '/banners/' . $bannerName,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Setting data updated',
            'data' => []
        ], 200);
    }
}
