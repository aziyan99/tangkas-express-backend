<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Destination;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AreaController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the Areas data.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $areas = Area::with('destinations')->latest()->get();
        $areaData = [];
        foreach ($areas as $area) {
            $destinationCount = Destination::where('area_id', $area->id)->get()->count();
            $area->destinationCount = $destinationCount;
            array_push($areaData, $area);
        }
        return response()->json([
            'success' => true,
            'message' => 'Areas data',
            'data' => $areaData
        ], 200);
    }

    /**
     * Store a newly created Area data in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'code' => 'required',
            'name' => 'required',
            'descriptions' => 'required'
        ]);
        Area::create($request->only('code', 'name', 'descriptions'));
        return response()->json([
            'success' => true,
            'message' => 'Area created',
            'data' => []
        ], 201);
    }

    /**
     * Display the specified Area data.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $area = Area::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Area data',
            'data' => $area
        ], 200);
    }

    /**
     * Update the specified Area data in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'code' => 'required',
            'name' => 'required',
            'descriptions' => 'required'
        ]);
        Area::with('destinations')->findOrFail($id)
            ->update($request->only('code', 'name', 'descriptions'));
        return response()->json([
            'success' => true,
            'message' => 'Area updated',
            'data' => []
        ], 200);
    }

    /**
     * Remove the specified Area data from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        Area::findOrFail($id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Area deleted',
            'data' => []
        ], 200);
    }
}
