<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\ShippingRoute;
use App\Models\ShippingService;
use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShippingRouteController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display list of Shipping routw data
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $shippingRoutes = ShippingRoute::with('shippingServices')->latest()->get();
        $shippingRouteData = [];
        foreach ($shippingRoutes as $shippingRoute) {
            $serviceCount = ShippingService::where('shipping_route_id', $shippingRoute->id)->get()->count();
            $shippingRoute->serviceCount = $serviceCount;
            array_push($shippingRouteData, $shippingRoute);
        }
        return response()->json([
            "success" => true,
            "message" => "Shipping route data",
            "data" => $shippingRouteData
        ], 200);
    }

    /**
     * Store new created Shipping route data
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'name' => 'required',
            'descriptions' => 'required',
        ]);
        $savedDate = ShippingRoute::create($request->only('name', 'descriptions'));
        if (!$savedDate) {
            return response()->json([
                "success" => false,
                "message" => "Failed to create shipping route",
                "data" => []
            ], 500);
        }

        return response()->json([
            "success" => true,
            "message" => "Shipping route created",
            "data" => []
        ], 201);
    }

    /**
     * Update spesific shipping route from resource
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'name' => 'required',
            'descriptions' => 'required',
        ]);
        $updatedShippingRoute = ShippingRoute::findOrFail($id);
        $updatedData = $updatedShippingRoute->update($request->only('name', 'descriptions'));
        if (!$updatedData) {
            return response()->json([
                "success" => false,
                "message" => "Failed to update shipping route",
                "data" => []
            ], 500);
        }
        return response()->json([
            "success" => true,
            "message" => "Shipping route updated",
            "data" => []
        ], 200);
    }

    /**
     * Show spesific shipping route data from resource
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $shippingRouteData = ShippingRoute::with('shippingServices')->findOrFail($id);
        return response()->json([
            "success" => true,
            "message" => "Shipping route data",
            "data" => $shippingRouteData
        ], 200);
    }

    /**
     * Destroy spesific shipping route data
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $shippingRouteData = ShippingRoute::findOrFail($id);
        $shippingRouteData->delete($id);
        return response()->json([
            "success" => true,
            "message" => "Shipping route deleted",
            "data" => []
        ], 200);
    }
}
