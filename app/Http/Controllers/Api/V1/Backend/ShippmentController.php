<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Area;
use App\Models\Destination;
use App\Models\Payment;
use App\Models\Receiver;
use App\Models\Sender;
use App\Models\Setting;
use App\Models\ShippingRoute;
use App\Models\Shippment;
use App\Models\Tracking;
use App\Models\User;
use Carbon\Carbon;
use ErrorException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShippmentController extends Controller
{

    /**
     * Create a new Shippment controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the Shippment resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if (auth('api')->user()->is_super_administrator != 1) {
            $shippments = [];
            return response()->json([
                'success' => true,
                'message' => 'Shippments data',
                'data' => $shippments
            ], 200);
        } else {
            $shippments = "";
            if ($request->keywords != null) {
                $shippments = Shippment::with(
                    'price',
                    'shippingService',
                    'sender',
                    'receiver',
                    'payment',
                    'trackings',
                    'originAgent',
                    'destinationAgent',
                    'inputBy',
                    'updateBy'
                )
                    ->where('receipt_number', 'like', '%' . $request->keywords . '%')
                    ->orWhere('shipping_type', 'like', '%' . $request->keywords . '%')
                    ->orWhere('shippment_date', 'like', '%' . $request->keywords . '%')
                    ->orWhere('delivered_date', 'like', '%' . $request->keywords . '%')
                    ->orWhere('status', 'like', '%' . $request->keywords . '%')
                    ->orWhere('colli', 'like', '%' . $request->keywords . '%')
                    ->latest()
                    ->get();
            } else {
                $shippments = Shippment::with(
                    'price',
                    'shippingService',
                    'sender',
                    'receiver',
                    'payment',
                    'trackings',
                    'originAgent',
                    'destinationAgent',
                    'inputBy',
                    'updateBy'
                )
                    ->latest()
                    ->get();
            }
            return response()->json([
                'success' => true,
                'message' => 'Shippments data',
                'data' => $shippments
            ], 200);
        }
    }

    public function getOutShippment(Request $request)
    {
        if ($request->keywords != null) {
            $shippments = Shippment::with(
                'price',
                'shippingService',
                'sender',
                'receiver',
                'payment',
                'trackings',
                'originAgent',
                'destinationAgent',
                'inputBy',
                'updateBy'
            )
                ->where('origin_agent_id', auth('api')->user()->agent_id)
                ->orWhere('receipt_number', 'like', '%' . $request->keywords . '%')
                ->orWhere('shipping_type', 'like', '%' . $request->keywords . '%')
                ->orWhere('status', 'like', '%' . $request->keywords . '%')
                ->orWhere('colli', 'like', '%' . $request->keywords . '%')
                ->latest()
                ->get();
            return response()->json([
                'success' => true,
                'message' => 'Shippments data',
                'data' => $shippments
            ], 200);
        } else {
            $shippments = Shippment::with(
                'price',
                'shippingService',
                'sender',
                'receiver',
                'payment',
                'trackings',
                'originAgent',
                'destinationAgent',
                'inputBy',
                'updateBy'
            )
                ->where('origin_agent_id', auth('api')->user()->agent_id)
                ->latest()
                ->get();
            return response()->json([
                'success' => true,
                'message' => 'Shippments data',
                'data' => $shippments
            ], 200);
        }
    }

    public function getInShippment(Request $request)
    {
        $count = $request->count ?? 10;
        //out
        if ($request->keywords != null) {
            $shippments = Shippment::with(
                'price',
                'shippingService',
                'sender',
                'receiver',
                'payment',
                'trackings',
                'originAgent',
                'destinationAgent',
                'inputBy',
                'updateBy'
            )
                ->where('destination_agent_id', auth('api')->user()->agent_id)
                ->orWhere('receipt_number', 'like', '%' . $request->keywords . '%')
                ->orWhere('shipping_type', 'like', '%' . $request->keywords . '%')
                ->orWhere('status', 'like', '%' . $request->keywords . '%')
                ->orWhere('colli', 'like', '%' . $request->keywords . '%')
                ->latest()
                ->get();
            return response()->json([
                'success' => true,
                'message' => 'Shippments data',
                'data' => $shippments
            ], 200);
        } else {
            $shippments = Shippment::with(
                'price',
                'shippingService',
                'sender',
                'receiver',
                'payment',
                'trackings',
                'originAgent',
                'destinationAgent',
                'inputBy',
                'updateBy'
            )
                ->where('destination_agent_id', auth('api')->user()->agent_id)
                ->latest()
                ->get();
            return response()->json([
                'success' => true,
                'message' => 'Shippments data',
                'data' => $shippments
            ], 200);
        }
    }

    /**
     * Store a newly created Shippment resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            "sender_name" => "required",
            "sender_phone_number" => "required",
            "sender_address" => "required",
            "receiver_name" => "required",
            "receiver_phone_number" => "required",
            "receiver_address" => "required",
            "price_id" => "required",
            "shipping_service_id" => "required",
            "shipping_type" => "required",
            "shipping_status" => "required",
            "colli" => "required",
            "descriptions" => "required",
            "weight" => "required",
            "payment_status" => "required",
            "total_price" => "required",
            "destination_agent_id" => "required",
            "discount" => "required",
            "insurance" => "required",
            "cargo_number" => "required",
            "packing" => "required"
        ]);

        $checkSender = $this->checkIfSenderExist($request->sender_name);
        $senderCode = $this->generateSenderCode();
        $sender = "";
        DB::transaction(function () use ($request, $sender, $senderCode, $checkSender) {
            if ($checkSender) {
                $sender = Sender::where('name', $request->sender_name)->first();
                if ($sender->phone_number != $request->sender_phone_number) {
                    $sender->update([
                        'phone_number' => $request->sender_phone_number,
                    ]);
                }
                if ($sender->address != $request->sender_address) {
                    $sender->update([
                        'address' => $request->sender_address,
                    ]);
                }
                $senderCode = $sender->code;
            } else {
                $sender = Sender::create([
                    'code' => $senderCode,
                    'name' => $request->sender_name,
                    'phone_number' => $request->sender_phone_number,
                    'address' => $request->sender_address
                ]);
            }

            $shippment = Shippment::create([
                'price_id' => $request->price_id,
                'shipping_service_id' => $request->shipping_service_id,
                'receipt_number' => $this->generateReciptNumber($request->destination_agent_id),
                'shipping_type' => $request->shipping_type,
                'shippment_date' => Carbon::now(),
                'status' => $request->shipping_status,
                'colli' => $request->colli,
                'descriptions' => $request->descriptions,
                'delivered_date' => '-',
                'cargo_number' => $request->cargo_number,
                'sender_id' => $sender->id,
                'origin_agent_id' => auth('api')->user()->agent_id,
                'destination_agent_id' => $request->destination_agent_id,
                'input_by_user_id' => auth('api')->user()->id,
                'update_by_user_id' => auth('api')->user()->id
            ]);

            Receiver::create([
                'shippment_id' => $shippment->id,
                'name' => $request->receiver_name,
                'phone_number' => $request->receiver_phone_number,
                'address' => $request->receiver_address
            ]);

            Payment::create([
                'shippment_id' => $shippment->id,
                'weight' => $request->weight,
                'status' => $request->payment_status,
                'insurance' => $request->insurance,
                'discount' => $request->discount,
                'total_price' => $request->total_price,
                'packing' => $request->packing,
            ]);

            $originAgent = Agent::findOrFail(auth('api')->user()->agent_id);

            Tracking::create([
                'shippment_id' => $shippment->id,
                'status' => "Paket telah diinput di agent " . $originAgent->destination->name,
            ]);
        });

        return response()->json([
            'success' => true,
            'message' => 'Shippment created',
            'data' => []
        ], 201);
    }

    /**
     * Display the specified Shippment resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        try {
            $shippment = Shippment::with(
                'price',
                'shippingService',
                'sender',
                'receiver',
                'payment',
                'trackings',
                'originAgent',
                'destinationAgent',
                'inputBy',
                'updateBy'
            )->findOrFail($id);
            $shippment->shipping_route = ShippingRoute::findOrFail($shippment->shippingService->shipping_route_id);
            $shippment->area = Area::findOrFail($shippment->destinationAgent->area_id);
            $shippment->destination = Destination::findOrFail($shippment->destinationAgent->destination_id);
            return response()->json([
                'success' => true,
                'message' => 'Shippment data',
                'data' => $shippment
            ], 200);
        } catch (ErrorException $exception) {
            $shippment = Shippment::with(
                'price',
                'shippingService',
                'sender',
                'receiver',
                'payment',
                'trackings',
                'inputBy',
                'updateBy'
            )->findOrFail($id);
            $shippment->shipping_route = ShippingRoute::findOrFail($shippment->shippingService->shipping_route_id);
            $shippment->area = [];
            $shippment->destination = [];
            return response()->json([
                'success' => true,
                'message' => 'Shippment data',
                'data' => $shippment
            ], 200);
        }
    }

    /**
     * Update the specified Shippment resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $request->validate([
            "receiver_name" => "required",
            "receiver_phone_number" => "required",
            "receiver_address" => "required",
            "shipping_type" => "required",
            "colli" => "required",
            "descriptions" => "required",
            "weight" => "required",
            "total_price" => "required",
            "cargo_number" => "required",
            "discount" => "required",
            "insurance" => "required",
            "packing" => "required"
        ]);

        Shippment::where('id', $id)->update([
            'shipping_type' => $request->shipping_type,
            'colli' => $request->colli,
            'descriptions' => $request->descriptions,
            'cargo_number' => $request->cargo_number,
            'update_by_user_id' => auth('api')->user()->id
        ]);

        Receiver::where('shippment_id', $id)->update([
            'name' => $request->receiver_name,
            'phone_number' => $request->receiver_phone_number,
            'address' => $request->receiver_address
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Shippment updated',
            'data' => []
        ], 200);
    }

    /**
     * Remove the specified Shippment resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        Shippment::findOrFail($id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Shippment deleted',
            'data' => []
        ], 200);
    }

    /**
     * Update shippment status
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function updateShippmentStatus(Request $request, int $id): JsonResponse
    {
        $request->validate([
            'status' => 'required'
        ]);

        Shippment::where('id', $id)->update([
            'status' => $request->status
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Shippment status updated',
            'data' => []
        ], 200);
    }

    /**
     * Update payment status in shippment
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function updatePaymentStatus(Request $request, int $id): JsonResponse
    {
        $request->validate([
            'status' => 'required'
        ]);

        Shippment::findOrFail($id)->update([
            'update_by_user_id' => auth('api')->user()->id,
        ]);

        Payment::where('shippment_id', $id)->update([
            'status' => $request->status
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Shippment payment status updated',
            'data' => []
        ], 200);
    }

    /**
     * Stream shipping receipt as pdf
     *
     * @param int $id
     * @return mixed
     */
    public function getShippmentReceipt(int $id)
    {
        $shippment = Shippment::with('price', 'shippingService', 'sender', 'receiver', 'payment')->findOrFail($id);
        $setting = Setting::first();
        $pdf = \PDF::loadView('receipt.receiptv2', compact('shippment', 'setting'));
        return $pdf->stream($shippment->receipt_number . '.pdf');
    }

    /**
     * Get available status of tracking shippment
     *
     * @param int $id
     * @return JsonResponse
     */
    public function getShippmentTrackingStatus(int $id): JsonResponse
    {
        $shippment = Shippment::with('receiver')->findOrFail($id);
        $originAgent = Agent::with('area', 'destination')->findOrFail($shippment->origin_agent_id);
        $destinationAgent = Agent::with('area', 'destination')->findOrFail($shippment->destination_agent_id);
        $data = [
            "Paket telah diinput di agent " . $originAgent->destination->name,
            "Paket telah keluar dari agent " . $originAgent->destination->name,
            "Paket telah diterima di agent " . $destinationAgent->destination->name,
            "Paket telah keluar di agent " . $destinationAgent->destination->name,
            "Paket diantar ke alamat " . $shippment->receiver->name,
            "Paket telah diterima oleh " . $shippment->receiver->name
        ];

        return response()->json([
            'success' => true,
            'message' => 'Available shippment tracking status',
            'data' => $data
        ], 200);
    }

    /**
     * Generate receipt number of shippment
     *
     * @param $destinationId
     * @return string
     */
    public function generateReciptNumber($destinationId): string
    {
        $month = date('m', time());
        $year = date('Y', time());
        $thisMonthReceipt = Shippment::whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->get()
            ->count();

        $currentReceipt = intval($thisMonthReceipt) + 1;
        $destinationAgent = Agent::findOrFail($destinationId);
        $destination = Destination::with('area')->findOrFail($destinationAgent->destination_id);
        $receiptNumberOnly = sprintf("%04s", $currentReceipt);
        $year = date('y', time());
        return $destination->area->code . $destination->code . $month . $year . $receiptNumberOnly;
    }

    /**
     * Generate sender code
     *
     * @return string
     */
    public function generateSenderCode(): string
    {
        $senderCount = Sender::all()->count();
        $senderCode = $senderCount + 1;
        $code = sprintf("%08s", $senderCode);
        return "SDR" . $code;
    }

    /**
     * Check if sender exist
     *
     * @param string $name
     * @return bool
     */
    public function checkIfSenderExist(string $name): bool
    {
        $sender = Sender::where('name', $name)->first();
        if ($sender == null) {
            return false;
        } else {
            return true;
        }
    }
}
