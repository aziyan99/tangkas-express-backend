<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $contacts = Contact::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Contacts data',
            'data' => $contacts
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'receipt_number' => 'required',
            'messages' => 'required'
        ]);

        Contact::create($request->only('name', 'email', 'phone_number', 'receipt_number', 'messages'));
        return response()->json([
            'success' => true,
            'message' => 'Contact created',
            'data' => []
        ], 201);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Contact $contact
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $contact = Contact::findOrFail($id);
        $contact->delete();
        return response()->json([
            'success' => true,
            'message' => 'Contact deleted',
            'data' => []
        ], 200);
    }
}
