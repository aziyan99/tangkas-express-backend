<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Destination;
use App\Models\Price;
use App\Models\Sender;
use App\Models\Setting;
use App\Models\Shippment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Get requirement data for shippment
 */
class MiscellaneousController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Search sender by name
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function searchSenderName(Request $request): JsonResponse
    {
        $senderName = $request->name;
        $senders = Sender::where('name', 'LIKE', '%' . $senderName . '%')->latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Senders data',
            'data' => $senders
        ], 200);
    }

    /**
     * Get available destinations in area
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getDestinationByAreaId(Request $request): JsonResponse
    {
        $areaId = $request->area_id;
        $destinations = Destination::with('area')->where('area_id', $areaId)->get();
        return response()->json([
            'success' => true,
            'message' => 'Destinations data',
            'data' => $destinations
        ], 200);
    }

    /**
     * Get available price in by area, destination, and service
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getPriceByAreaDestinationService(Request $request): JsonResponse
    {
        $areaId = $request->area_id;
        $destinationId = $request->destination_id;
        $serviceId = $request->service_id;

        $price = Price::where('area_id', $areaId)
            ->where('destination_id', $destinationId)
            ->where('shipping_service_id', $serviceId)
            ->first();

        return response()->json([
            'success' => true,
            'message' => 'Price data',
            'data' => $price
        ], 200);
    }

    /**
     * Get available agent by area, and destination
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAgentByAreaAndDestination(Request $request): JsonResponse
    {
        $areaId = $request->area_id;
        $destinationId = $request->destination_id;

        $agent = Agent::where('area_id', $areaId)
            ->where('destination_id', $destinationId)
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Agent data',
            'data' => $agent
        ], 200);
    }

    /**
     * Get available agents in area
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getAgentByArea(Request $request): JsonResponse
    {
        $areaId = $request->area_id;

        $agents = Agent::where('area_id', $areaId)->get();

        return response()->json([
            'success' => true,
            'message' => 'Agents data',
            'data' => $agents
        ], 200);
    }

    /**
     * Get all sender data
     *
     * @return JsonResponse
     */
    public function getSender(): JsonResponse
    {
        $senders = Sender::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Senders data',
            'data' => $senders
        ], 200);
    }

    /**
     * Get detail of sender
     *
     * @param int $id
     * @return JsonResponse
     */
    public function getSenderDetail(int $id): JsonResponse
    {
        $sender = Sender::with('shippment')->findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Senders detail data',
            'data' => $sender
        ], 200);
    }

    /**
     * Print manifest
     *
     * @param int $id
     * @return mixed
     */
    public function printManifest(Request $request)
    {
        $request->validate([
            'destination_agent_id' => 'required'
        ]);
        $originAgent = Agent::findOrFail(auth()->user()->agent_id);
        $destinationAgent = Agent::findOrFail($request->destination_agent_id);
        $shippments = Shippment::with('originAgent', 'destinationAgent', 'shippingService', 'sender', 'receiver', 'payment')
            ->where('origin_agent_id', '=', auth()->user()->agent_id)
            ->where('destination_agent_id', '=', $request->destination_agent_id)
            ->get();
        $setting = Setting::first();
        $title = 'manifest-' . $originAgent->name . "-" . $destinationAgent->name . '.pdf';
        $pdf = \PDF::loadView('manifest.manifestv1', compact('shippments', 'originAgent', 'destinationAgent', 'title', 'setting'));
        $pdf->setPaper('a3', 'landscape');
        return $pdf->stream($title);
    }

    /**
     * Get coming shippment
     *
     * @return JsonResponse
     */
    public function getInUndoneShippment(): JsonResponse
    {
        $shippments = Shippment::with('receiver', 'originAgent', 'trackings')->where('destination_agent_id', auth()->user()->agent_id)
            ->where('status', '!=', 'delivered')
            ->latest()
            ->get();
        return response()->json([
            'success' => true,
            'message' => 'In undone shippment data',
            'data' => $shippments
        ], 200);
    }

    /**
     * Get out shippment
     *
     * @return JsonResponse
     */
    public function getOutUndoneShippment(): JsonResponse
    {
        $shippments = Shippment::with('sender', 'destinationAgent', 'trackings')->where('origin_agent_id', auth()->user()->agent_id)
            ->where('status', '!=', 'delivered')
            ->latest()
            ->get();
        return response()->json([
            'success' => true,
            'message' => 'Out undone shippment data',
            'data' => $shippments
        ], 200);
    }

    public function getDashboardData()
    {
        $shippingDone = Shippment::where('status', '=', 'delivered')->get()->count();
        $shippingUnDone = Shippment::where('status', '!=', 'delivered')->get()->count();
        $agentTotal = Agent::all()->count();
        $senderTotal = Sender::all()->count();

        return response()->json([
            'success' => true,
            'message' => 'Dashboard data',
            'data' => [
                'shippingDoneCount' => $shippingDone,
                'shippingUnDoneCount' => $shippingUnDone,
                'agentTotal' => $agentTotal,
                'senderTotal' => $senderTotal
            ]
        ], 200);
    }
}
