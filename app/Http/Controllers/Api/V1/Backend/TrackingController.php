<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Shippment;
use App\Models\Tracking;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TrackingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Store a newly created shippment tracking.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'shippment_id' => 'required',
            'shippment_status' => 'required',
            'status' => 'required'
        ]);

        Shippment::findOrFail($request->shippment_id)->update([
            'status' => $request->shippment_status,
            'update_by_user_id' => auth('api')->user()->id,
        ]);

        Tracking::create($request->only('shippment_id', 'status'));

        return response()->json([
            'success' => true,
            'message' => 'Tracking created!',
            'data' => []
        ], 201);
    }
}
