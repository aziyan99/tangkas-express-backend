<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AgentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the Agents data.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $agents = Agent::with('area', 'destination', 'users')->latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Agents data',
            'data' => $agents
        ], 200);
    }

    /**
     * Store a newly created  Agents data in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'area_id' => 'required',
            'destination_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'descriptions' => 'required',
            'is_main_agent' => 'required',
            'phone_number' => 'required'
        ]);

        Agent::create($request->only(
            'area_id',
            'destination_id',
            'name',
            'address',
            'descriptions',
            'is_main_agent',
            'phone_number'
        ));

        return response()->json([
            'success' => true,
            'message' => 'Agent created',
            'data' => []
        ], 201);
    }

    /**
     * Display the specified  Agents data.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $agent = Agent::with('area', 'destination', 'users')->findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'User data',
            'data' => $agent
        ], 200);
    }

    /**
     * Update the specified  Agents data in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'area_id' => 'required',
            'destination_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'descriptions' => 'required',
            'is_main_agent' => 'required',
            'phone_number' => 'required'
        ]);

        Agent::findOrFail($id)
            ->update($request->only(
                'area_id',
                'destination_id',
                'name',
                'address',
                'descriptions',
                'is_main_agent',
                'phone_number'
            ));

        return response()->json([
            'success' => true,
            'message' => 'Agent updated',
            'data' => []
        ], 200);
    }

    /**
     * Remove the specified  Agents data from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        Agent::findOrFail($id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Agent deleted',
            'data' => []
        ], 200);
    }
}
