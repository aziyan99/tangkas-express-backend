<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\AvailableService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AvailableServiceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the Available service data.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $services = AvailableService::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Available services data',
            'data' => $services
        ], 200);
    }

    /**
     * Store a newly created Available service data in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'logo' => 'required',
            'description' => 'required',
            'estimations' => 'required',
            'area' => 'required',
            'name' => 'required'
        ]);
        //convert base64 to image and save it
        $image_64 = $request->logo;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        // find substring for replace here eg: data:image/png;base64,
        $logo = str_replace($replace, '', $image_64);
        $logo = str_replace(' ', '+', $logo);
        $imageName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/services/' . $imageName, base64_decode($logo));

        AvailableService::create([
            'logo' => '/services/' . $imageName,
            'description' => $request->description,
            'estimations' => $request->estimations,
            'area' => $request->area,
            'name' => $request->name
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Available services created',
            'data' => []
        ], 201);
    }

    /**
     * Display the specified Available service data.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $service = AvailableService::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Available service data',
            'data' => $service
        ], 200);
    }

    /**
     * Update the specified Available service data in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'description' => 'required',
            'estimations' => 'required',
            'area' => 'required',
        ]);

        $availableService = AvailableService::findOrFail($id);
        //convert base64 to image and save it
        if ($request->logo) {
            $image_64 = $request->logo;
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
            $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
            // find substring for replace here eg: data:image/png;base64,
            $logo = str_replace($replace, '', $image_64);
            $logo = str_replace(' ', '+', $logo);
            $imageName = uniqid() . '.' . $extension;

            Storage::disk('public')->put('/services/' . $imageName, base64_decode($logo));

            if (File::exists(public_path($availableService->logo))) {
                File::delete(public_path($availableService->logo));
            }

            $availableService->update([
                'logo' => '/services/' . $imageName,
                'description' => $request->description,
                'estimations' => $request->estimations,
                'area' => $request->area,
                'name' => $request->name,
            ]);
        } else {
            $availableService->update([
                'description' => $request->description,
                'estimations' => $request->estimations,
                'area' => $request->area,
                'name' => $request->name,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Available services updated',
            'data' => []
        ], 200);
    }

    /**
     * Remove the specified Available service data from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        AvailableService::findOrFail($id)->delete();

        return response()->json([
            'success' => true,
            'message' => 'Available services deleted',
            'data' => []
        ], 200);
    }
}
