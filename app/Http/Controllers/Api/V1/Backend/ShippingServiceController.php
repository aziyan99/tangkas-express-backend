<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\ShippingService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShippingServiceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the shipping services data.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $shippingService = ShippingService::with('shippingRoute')->latest()->get();
        return response()->json([
            "success" => true,
            "message" => "Shipping service data",
            "data" => $shippingService
        ], 200);
    }

    /**
     * Store a newly created shipping service data in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'shipping_route_id' => 'required',
            'name' => 'required',
            'descriptions' => 'required',
        ]);
        $savedData = ShippingService::create($request->only('shipping_route_id', 'name', 'descriptions'));
        if (!$savedData) {
            return response()->json([
                "success" => false,
                "message" => "Failed to create shipping service",
                "data" => []
            ], 500);
        }

        return response()->json([
            "success" => true,
            "message" => "Shipping service created",
            "data" => []
        ], 201);
    }

    /**
     * Display the specified shipping service data.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $shippingServiceData = ShippingService::with('shippingRoute')->findOrFail($id);
        return response()->json([
            "success" => true,
            "message" => "Shipping service data",
            "data" => $shippingServiceData
        ], 200);
    }

    /**
     * Update the specified shipping service data in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'shipping_route_id' => 'required',
            'name' => 'required',
            'descriptions' => 'required',
        ]);
        ShippingService::findOrFail($id)
            ->update($request->only('shipping_route_id', 'name', 'descriptions'));
        return response()->json([
            'success' => true,
            'message' => 'shipping service updated',
            'data' => []
        ], 200);
    }

    /**
     * Remove the specified shipping service data from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        ShippingService::findOrFail($id)
            ->delete();
        return response()->json([
            'success' => true,
            'message' => 'shipping service deleted',
            'data' => []
        ], 200);
    }
}
