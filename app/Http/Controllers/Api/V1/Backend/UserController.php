<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the user data.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {

        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $users = User::with('agent')->latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Users data',
            'data' => $users
        ], 200);
    }

    /**
     * Store a newly created user data in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {

        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        // email unique
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
            'agent_id' => 'required',
            'image' => 'required',
            'role' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
            'is_super_administrator' => 'required',
            'is_active' => 'required'
        ]);

        //convert base64 to image and save it
        $image_64 = $request->image;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        // find substring for replace here eg: data:image/png;base64,
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        $imageName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/profile_images/' . $imageName, base64_decode($image));

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'agent_id' => $request->agent_id,
            'image' => '/profile_images/' . $imageName,
            'role' => $request->role,
            'address' => $request->address,
            'phone_number' => $request->phone_number,
            'is_super_administrator' => $request->is_super_administrator,
            'is_active' => $request->is_active,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'User created',
            'data' => []
        ], 201);
    }

    /**
     * Display the specified user data.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {

        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $user = User::with('agent')->findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'User data',
            'data' => $user
        ], 200);
    }

    /**
     * Update the specified user data in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {

        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        // email unique
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id,
            'image' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
            'is_active' => 'required'
        ]);

        //convert base64 to image and save it
        $image_64 = $request->image;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        // find substring for replace here eg: data:image/png;base64,
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', '+', $image);
        $imageName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/profile_images/' . $imageName, base64_decode($image));

        $user = User::findOrFail($id);
        if (Storage::disk('public')->exists($user->image)) {
            Storage::disk('pulic')->delete($user->image);
        }

        User::findOrFail($id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,
                'image' => '/profile_images/' . $imageName,
                'address' => $request->address,
                'phone_number' => $request->phone_number,
                'is_active' => $request->is_active,
            ]);

        return response()->json([
            'success' => true,
            'message' => 'User updated',
            'data' => []
        ], 200);
    }

    /**
     * Remove the specified user data from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {

        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        User::findOrFail($id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'User deleted',
            'data' => []
        ], 200);
    }

    /**
     * Reset user password to email
     *
     * @param Request $request
     * @return void
     */
    public function resetPassword(Request $request)
    {

        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'user_id' => 'required'
        ]);
        $user = User::findOrFail($request->user_id);
        $user->update([
            'password' => Hash::make($user->email)
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Password reseted',
            'data' => []
        ], 200);
    }

    /**
     * Reset user status
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateUserStatus(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'is_active' => 'required',
            'user_id' => 'required'
        ]);

        User::findOrFail($request->user_id)->update([
            'is_active' => $request->is_active
        ]);
        return response()->json([
            'success' => true,
            'message' => 'User status updated',
            'data' => []
        ], 200);
    }

    /**
     * Update user agent
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateUserAgent(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        $request->validate([
            'agent_id' => 'required',
            'user_id' => 'required'
        ]);
        User::findOrFail($request->user_id)->update([
            'agent_id' => $request->agent_id
        ]);
        return response()->json([
            'success' => true,
            'message' => 'User agent updated',
            'data' => []
        ], 200);
    }
}
