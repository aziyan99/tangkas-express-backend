<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Price;
use App\Models\ShippingRoute;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PriceController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the prices data.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $prices = Price::with(
            'shippingService',
            'area',
            'destination',
            'shippment'
        )->latest()->get();
        $pricesData = [];
        foreach ($prices as $price) {
            $routeData = ShippingRoute::findOrFail($price->shippingService->shipping_route_id);
            $price->shipping_route = $routeData;
            array_push($pricesData, $price);
        }
        return response()->json([
            'success' => true,
            'message' => 'Prices data',
            'data' => $pricesData
        ], 200);
    }

    /**
     * Store a newly created prices data in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'shipping_service_id' => 'required',
            'area_id' => 'required',
            'destination_id' => 'required',
            'min_weight' => 'required',
            'price' => 'required',
            'lead_time' => 'required',
            'descriptions' => 'required'
        ]);
        Price::create($request->only('shipping_service_id', 'area_id', 'destination_id', 'min_weight', 'price', 'lead_time', 'descriptions'));

        return response()->json([
            'success' => true,
            'message' => 'Price created',
            'data' => []
        ], 201);
    }

    /**
     * Display the specified prices data.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $price = Price::with(
            'shippingService',
            'area',
            'destination',
            'shippment'
        )->findOrFail($id);
        $price->shipping_route = ShippingRoute::findOrFail($price->shippingService->shipping_route_id);
        return response()->json([
            'success' => true,
            'message' => 'Price data',
            'data' => $price
        ], 200);
    }

    /**
     * Update the specified prices data in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'shipping_service_id' => 'required',
            'area_id' => 'required',
            'destination_id' => 'required',
            'min_weight' => 'required',
            'price' => 'required',
            'lead_time' => 'required',
            'descriptions' => 'required'
        ]);
        Price::findOrFail($id)
            ->update($request->only('shipping_service_id', 'area_id', 'destination_id', 'min_weight', 'price', 'lead_time', 'descriptions'));
        return response()->json([
            'success' => true,
            'message' => 'Price updated',
            'data' => []
        ], 200);
    }

    /**
     * Remove the specified prices data from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        Price::findOrFail($id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Price deleted',
            'data' => []
        ], 200);
    }
}
