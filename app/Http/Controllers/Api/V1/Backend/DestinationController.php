<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Destination;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DestinationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the destinations data.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $destinations = Destination::with('area')->latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'Destinations data',
            'data' => $destinations
        ], 200);
    }

    /**
     * Store a newly created destination data in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'area_id' => 'required',
            'code' => 'required',
            'name' => 'required',
            'postal_code' => 'required',
            'descriptions' => 'required'
        ]);
        Destination::create($request->only('area_id', 'code', 'name', 'descriptions', 'postal_code'));
        return response()->json([
            'success' => true,
            'message' => 'Destination created',
            'data' => []
        ], 201);
    }

    /**
     * Display the specified destination data.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $destination = Destination::with('area')->findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Destination data',
            'data' => $destination
        ], 200);
    }

    /**
     * Update the specified destination data in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin') {
            abort(403);
        }
        $request->validate([
            'area_id' => 'required',
            'code' => 'required',
            'name' => 'required',
            'postal_code' => 'required',
            'descriptions' => 'required'
        ]);
        Destination::findOrFail($id)->update($request->only('area_id', 'code', 'name', 'descriptions', 'postal_code'));
        return response()->json([
            'success' => true,
            'message' => 'Destination updated',
            'data' => []
        ], 200);
    }

    /**
     * Remove the specified destination data from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        if (auth('api')->user()->role != 'admin' && auth('api')->user()->is_super_administrator != 1) {
            abort(403);
        }
        Destination::findOrFail($id)->delete();
        return response()->json([
            'success' => true,
            'message' => 'Destination deleted',
            'data' => []
        ], 200);
    }
}
