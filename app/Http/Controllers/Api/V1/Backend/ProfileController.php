<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Update user basic information
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateProfileInfo(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,' . auth('api')->user()->id,
            'address' => 'required',
            'phone_number' => 'required',
        ]);

        $profile = User::findOrFail(auth('api')->user()->id);
        $profile->update($request->only('name', 'email', 'address', 'phone_number'));

        return response()->json([
            'success' => true,
            'message' => 'Profile updated!',
            'data' => []
        ], 200);
    }

    /**
     * Update user profile image
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateProfileImage(Request $request): JsonResponse
    {
        $request->validate([
            'image' => 'required'
        ]);

        $profile = User::findOrFail(auth('api')->user()->id);
        //convert base64 to image and save it
        $image_64 = $request->image;
        $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
        $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
        // find substring for replace here eg: data:image/png;base64,
        $image = str_replace($replace, '', $image_64);
        $image = str_replace(' ', ' + ', $image);
        $imageName = uniqid() . '.' . $extension;

        Storage::disk('public')->put('/profile_images/' . $imageName, base64_decode($image));

        if (Storage::disk('public')->exists($profile->image)) {
            Storage::disk('pulic')->delete($profile->image);
        }

        $profile->update([
            'image' => '/profile_images/' . $imageName,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Profile updated!',
            'data' => []
        ], 200);
    }

    /**
     * Update user password
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePassword(Request $request): JsonResponse
    {
        $request->validate([
            'new_password' => 'required'
        ]);

        User::findOrFail(auth('api')->user()->id)->update([
            'password' => Hash::make($request->new_password),
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Password updated!',
            'data' => []
        ], 200);
    }
}
