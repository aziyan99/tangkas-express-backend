<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Exports\AgentExport;
use App\Exports\SenderExport;
use App\Exports\ShippmentExport;
use App\Http\Controllers\Controller;
use App\Models\Agent;
use App\Models\Sender;
use App\Models\Shippment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ReportController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Export sender data
     *
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function senderReport(Request $request)
    {
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
            'type' => 'required'
        ]);

        $startDate = Carbon::createFromFormat('Y-m-d', $request->start_date);
        $endDate = Carbon::createFromFormat('Y-m-d', $request->end_date);
        $senders = Sender::whereBetween('created_at', [$startDate, $endDate])->get();
        $title = 'Laporan_Pengirim-' . time();

        if ($request->type == "pdf") {
            $pdf = \PDF::loadView('reports.sender', compact('senders', 'title'));
            $pdf->setPaper('a3', 'potrait');
            return $pdf->stream($title . '.pdf');
        } else {
            return Excel::download(new SenderExport($request->start_date, $request->end_date, $title), $title . '.xlsx');
        }
    }

    /**
     * Export shippment data
     *
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function shippmentReport(Request $request)
    {
        $request->validate([
            'start_date' => 'required',
            'end_date' => 'required',
            'type' => 'required'
        ]);

        $startDate = Carbon::createFromFormat('Y-m-d', $request->start_date);
        $endDate = Carbon::createFromFormat('Y-m-d', $request->end_date);
        $title = 'Laporan_Pengiriman-' . time();
        $shippments = Shippment::with(
            'originAgent',
            'destinationAgent',
            'sender',
            'receiver',
            'payment'
        )
            ->whereBetween('created_at', [$startDate, $endDate])->get();

        if ($request->type == "pdf") {
            $pdf = \PDF::loadView('reports.shippment', compact('shippments', 'title'));
            $pdf->setPaper('a3', 'landscape');
            return $pdf->stream($title . '.pdf');
        } else {
            return Excel::download(new ShippmentExport($request->start_date, $request->end_date, $title), $title . '.xlsx');
        }
    }

    /**
     * Export agent data
     *
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function agentReport(Request $request)
    {
        $request->validate([
            'type' => 'required'
        ]);

        $title = 'Laporan_Agent-' . time();
        $agents = Agent::with('area', 'destination')->latest()->get();
        if ($request->type == "pdf") {
            $pdf = \PDF::loadView('reports.agent', compact('agents', 'title'));
            $pdf->setPaper('a3', 'landscape');
            return $pdf->stream($title . '.pdf');
        } else {
            return Excel::download(new AgentExport($title), $title . '.xlsx');
        }
    }
}
