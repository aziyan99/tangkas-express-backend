<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Sender extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'name',
        'phone_number',
        'address',
    ];

    /**
     * Relation with shippment
     *
     * @return HasMany
     */
    public function shippment(): HasMany
    {
        return $this->hasMany(Shippment::class);
    }
}
