<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Price extends Model
{
    use HasFactory;

    protected $fillable = [
        'shipping_service_id',
        'area_id',
        'destination_id',
        'min_weight',
        'price',
        'lead_time',
        'descriptions',
    ];

    /**
     * Relation with shipping service model
     *
     * @return BelongsTo
     */
    public function shippingService(): BelongsTo
    {
        return $this->belongsTo(ShippingService::class);
    }

    /**
     * Relation with area model
     *
     * @return BelongsTo
     */
    public function area(): BelongsTo
    {
        return $this->belongsTo(Area::class);
    }

    /**
     * Relation with destination model
     *
     * @return BelongsTo
     */
    public function destination(): BelongsTo
    {
        return $this->belongsTo(Destination::class);
    }

    /**
     * Relation with shippment model
     *
     * @return HasMany
     */
    public function shippment(): HasMany
    {
        return $this->hasMany(Shippment::class);
    }
}
