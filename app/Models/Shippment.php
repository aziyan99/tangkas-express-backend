<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Shippment extends Model
{
    use HasFactory;

    protected $fillable = [
        'price_id',
        'shipping_service_id',
        'receipt_number',
        'shipping_type',
        'shippment_date',
        'delivered_date',
        'status',
        'colli',
        'descriptions',
        'cargo_number',
        'sender_id',
        'origin_agent_id',
        'destination_agent_id',
        'input_by_user_id',
        'update_by_user_id'
    ];

    /**
     * Relation with prices model
     *
     * @return BelongsTo
     */
    public function price(): BelongsTo
    {
        return $this->belongsTo(Price::class);
    }

    /**
     * Relation with shipping service model
     *
     * @return BelongsTo
     */
    public function shippingService(): BelongsTo
    {
        return $this->belongsTo(ShippingService::class);
    }

    /**
     * Relation with sender model
     *
     * @return BelongsTo
     */
    public function sender(): BelongsTo
    {
        return $this->belongsTo(Sender::class);
    }

    /**
     * Relation with receiver model
     *
     * @return HasOne
     */
    public function receiver(): HasOne
    {
        return $this->hasOne(Receiver::class);
    }

    /**
     * Relation with payment model
     *
     * @return HasOne
     */
    public function payment(): HasOne
    {
        return $this->hasOne(Payment::class);
    }

    /**
     * Relation with tracking model
     *
     * @return HasMany
     */
    public function trackings(): HasMany
    {
        return $this->hasMany(Tracking::class);
    }

    /**
     * Accessor to format date
     *
     * @param $value
     * @return string
     */
    public function getShippmentDateAttribute($value): string
    {
        $date = Carbon::create($value);
        return $date->format('d M y');
    }

    /**
     * Relation with origin agent
     *
     * @return belongsTo
     */
    public function originAgent(): BelongsTo
    {
        return $this->belongsTo(Agent::class, 'origin_agent_id');
    }

    /**
     * Relation with destination agent
     *
     * @return belongsTo
     */
    public function destinationAgent(): BelongsTo
    {
        return $this->belongsTo(Agent::class, 'destination_agent_id');
    }

    /**
     * Relation with user
     *
     * @return belongsTo
     */
    public function inputBy()
    {
        return $this->belongsTo(User::class, 'input_by_user_id');
    }

    /**
     *  Relation with user
     *
     * @return belongsTo
     */
    public function updateBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'update_by_user_id');
    }

    /**
     * Format date
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value): string
    {
        $createdDate = Carbon::create($value);
        return $createdDate->timezone('Asia/Jakarta')->format('d M Y H:i:s');
    }

    /**
     * Format date
     *
     * @param $value
     * @return string
     */
    public function getUpdatedAtAttribute($value): string
    {
        $createdDate = Carbon::create($value);
        return $createdDate->timezone('Asia/Jakarta')->format('d M Y h:i:s');
    }
}
