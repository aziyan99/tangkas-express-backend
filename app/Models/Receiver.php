<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Receiver extends Model
{
    use HasFactory;

    protected $fillable = [
        'shippment_id',
        'name',
        'phone_number',
        'address',
    ];

    /**
     * Relation with shippment model
     *
     * @return BelongsTo
     */
    public function shippment(): BelongsTo
    {
        return $this->belongsTo(Shippment::class);
    }
}
