<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Tracking extends Model
{
    use HasFactory;

    protected $fillable = [
        'shippment_id', 'status'
    ];

    /**
     * Relation with shippment
     *
     * @return BelongsTo
     */
    public function shippment(): BelongsTo
    {
        return $this->belongsTo(Shippment::class);
    }

    /**
     * Format date
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value): string
    {
        $createdDate = Carbon::create($value);
        return $createdDate->timezone('Asia/Jakarta')->format('d M Y h:i:s');
    }
}
