<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = [
        'shippment_id',
        'weight',
        'status',
        'total_price',
        'discount',
        'insurance',
        'packing'
    ];

    /**
     * Relation with shippment
     *
     * @return BelongsTo
     */
    public function shippment(): BelongsTo
    {
        return $this->belongsTo(Shippment::class);
    }
}
