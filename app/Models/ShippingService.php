<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ShippingService extends Model
{
    use HasFactory;

    protected $fillable = [
        'shipping_route_id',
        'name',
        'descriptions'
    ];

    /**
     * Relation with shipping route model
     *
     * @return BelongsTo
     */
    public function shippingRoute(): BelongsTo
    {
        return $this->belongsTo(ShippingRoute::class);
    }

    /**
     * Relation with price model
     *
     * @return HasMany
     */
    public function prices(): HasMany
    {
        return $this->hasMany(Price::class);
    }

    /**
     * Relation with shippment model
     *
     * @return HasMany
     */
    public function shippment(): HasMany
    {
        return $this->hasMany(Shippment::class);
    }
}
