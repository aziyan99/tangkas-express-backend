<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'email', 'phone_number', 'receipt_number', 'messages'
    ];

    /**
     * Format date
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value): string
    {
        $createdDate = Carbon::create($value);
        return $createdDate->format('d M Y h:i:s A');
    }
}
