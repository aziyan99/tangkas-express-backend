<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ShippingRoute extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'descriptions'
    ];

    /**
     * Relation with shippinh service model
     *
     * @return HasMany
     */
    public function shippingServices(): HasMany
    {
        return $this->hasMany(ShippingService::class);
    }
}
