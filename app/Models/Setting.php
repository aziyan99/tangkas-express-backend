<?php

namespace App\Models;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    protected $fillable = [
        'logo',
        'phone_number',
        'email',
        'instagram',
        'facebook',
        'address',
        'name',
        'visi',
        'misi',
        'privacy_policy',
        'about',
        'banner_one',
        'banner_two',
        'banner_three',
        'banner_four',
        'logo_pdf',
        'service_banner',
        'price_banner',
        'receipt_banner',
        'about_banner',
        'contact_banner'
    ];

    public function getLogoPdfAttribute($value)
    {
        $path = storage_path('app/public' . $value);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        try {
            $data = file_get_contents($path);
            return 'data:image/' . $type . ';base64,' . base64_encode($data);
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * Mutator method to return the url of logo
     *
     * @param $value
     * @return Application|UrlGenerator|string
     */
    public function getLogoAttribute($value)
    {
        return url('storage' . $value);
    }

    /**
     * Mutator method to return the url of banner 1
     *
     * @param $value
     * @return Application|UrlGenerator|string
     */
    public function getBannerOneAttribute($value)
    {
        return url('storage' . $value);
    }


    /**
     * Mutator method to return the url of banner 2
     *
     * @param $value
     * @return Application|UrlGenerator|string
     */
    public function getBannerTwoAttribute($value)
    {
        return url('storage' . $value);
    }


    /**
     * Mutator method to return the url of banner 3
     *
     * @param $value
     * @return Application|UrlGenerator|string
     */
    public function getBannerThreeAttribute($value)
    {
        return url('storage' . $value);
    }


    /**
     * Mutator method to return the url of banner 4
     *
     * @param $value
     * @return Application|UrlGenerator|string
     */
    public function getBannerFourAttribute($value)
    {
        return url('storage' . $value);
    }

    public function getServiceBannerAttribute($value)
    {
        return url('storage' . $value);
    }

    public function getPriceBannerAttribute($value)
    {
        return url('storage' . $value);
    }

    public function getReceiptBannerAttribute($value)
    {
        return url('storage' . $value);
    }

    public function getContactBannerAttribute($value)
    {
        return url('storage' . $value);
    }

    public function getAboutBannerAttribute($value)
    {
        return url('storage' . $value);
    }
}
