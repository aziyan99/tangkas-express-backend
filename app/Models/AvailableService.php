<?php

namespace App\Models;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvailableService extends Model
{
    use HasFactory;

    protected $fillable = [
      'logo', 'description', 'estimations', 'area', 'name'
    ];

    /**
     * Accessor to access the url of logo
     *
     * @param $value
     * @return Application|UrlGenerator|string
     */
    public function getLogoAttribute($value)
    {
        return url('storage' . $value);
    }
}
