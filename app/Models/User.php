<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'agent_id',
        'image',
        'role',
        'address',
        'phone_number',
        'is_super_administrator',
        'is_active'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * Define image with url
     *
     * @param [type] $value
     * @return Application|UrlGenerator|string
     */
    public function getImageAttribute($value)
    {
        return url('storage' . $value);
    }

    /**
     * Get user status
     *
     * @param [type] $value
     * @return string
     */
    public function getIsActiveAttribute($value): string
    {
        return ($value == 1) ? 'Aktif' : 'Suspend';
    }

    /**
     * Format date
     *
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value): string
    {
        $createdDate = Carbon::create($value);
        return $createdDate->timezone('Asia/Jakarta')->format('d M Y H:i:s');
    }
}
