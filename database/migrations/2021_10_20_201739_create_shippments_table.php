<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('price_id')
                ->nullable()
                ->references('id')
                ->on('prices')
                ->onDelete('SET NULL')
                ->onUpdate('CASCADE');
            $table->foreignId('shipping_service_id')
                ->nullable()
                ->references('id')
                ->on('shipping_services')
                ->onDelete('SET NULL')
                ->onUpdate('CASCADE');
            $table->string('receipt_number');
            $table->string('shipping_type');
            $table->string('shippment_date');
            $table->string('delivered_date');
            $table->enum('status', ['input', 'in', 'out', 'delivered']);
            $table->integer('colli');
            $table->longText('descriptions');
            $table->foreignId('origin_agent_id')
                ->nullable()
                ->references('id')
                ->on('agents')
                ->onUpdate('CASCADE')
                ->nullOnDelete();
            $table->foreignId('destination_agent_id')
                ->nullable()
                ->references('id')
                ->on('agents')
                ->onUpdate('CASCADE')
                ->nullOnDelete();
            $table->foreignId('input_by_user_id')
                ->nullable()
                ->references('id')
                ->on('users')
                ->onUpdate('CASCADE')
                ->nullOnDelete();
            $table->foreignId('update_by_user_id')
                ->nullable()
                ->references('id')
                ->on('users')
                ->onUpdate('CASCADE')
                ->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shippments');
    }
}
