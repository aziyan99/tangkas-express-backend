<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('area_id')
                ->references('id')
                ->on('areas')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreignId('destination_id')
                ->references('id')
                ->on('destinations')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->string('name');
            $table->longText('address');
            $table->longText('descriptions');
            $table->integer('is_main_agent')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
