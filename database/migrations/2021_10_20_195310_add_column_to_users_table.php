<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('agent_id')
                ->nullable()
                ->references('id')
                ->on('agents')
                ->nullOnDelete()
                ->onUpdate('CASCADE');
            $table->string('image')->nullable();
            $table->enum('role', ['admin', 'agent'])->default('admin');
            $table->longText('address')->nullable();
            $table->string('phone_number')->nullable();
            $table->integer('is_super_administrator')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('agent_id');
            $table->dropColumn('image');
            $table->dropColumn('role');
            $table->dropColumn('address');
            $table->dropColumn('phone_number');
            $table->dropColumn('is_super_administrator');
            $table->dropColumn('is_active');
        });
    }
}
