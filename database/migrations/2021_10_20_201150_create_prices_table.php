<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('shipping_service_id')
                ->references('id')
                ->on('shipping_services')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreignId('area_id')
                ->references('id')
                ->on('areas')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreignId('destination_id')
                ->references('id')
                ->on('destinations')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->string('min_weight');
            $table->string('price');
            $table->string('lead_time');
            $table->longText('descriptions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
