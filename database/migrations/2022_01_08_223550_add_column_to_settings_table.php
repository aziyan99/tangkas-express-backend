<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('service_banner')->nullable();
            $table->string('receipt_banner')->nullable();
            $table->string('price_banner')->nullable();
            $table->string('about_banner')->nullable();
            $table->string('contact_banner')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('service_banner');
            $table->dropColumn('receipt_banner');
            $table->dropColumn('price_banner');
            $table->dropColumn('about_banner');
            $table->dropColumn('contact_banner');
        });
    }
}
