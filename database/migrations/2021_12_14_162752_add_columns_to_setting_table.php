<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->string('banner_one')->nullable();
            $table->string('banner_two')->nullable();
            $table->string('banner_three')->nullable();
            $table->string('banner_four')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('banner_1');
            $table->dropColumn('banner_2');
            $table->dropColumn('banner_3');
            $table->dropColumn('banner_4');
        });
    }
}
